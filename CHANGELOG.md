# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.0.3] - 2018-09-10
### Changed
- Add forgot password and reset functionallity

## [v1.0.2] - 2018-09-10
### Changed
- Change phpmailer, upload and recaptcha libs to composer

## [v1.0.1] - 2018-06-15
### Added
- This CHANGELOG file to hopefully serve as an evolving CHANGELOG.
- Autosugested search
- Sucursales module

### Changed
- Update DB schema to NotORM
- Update Bootstrap to v4.0.0-beta.2

## [v1.0.0] - 2017-12-18
### Added
- Initial commit

## [Unreleased]
- TO DO






Added - Changed - Fixed - Removed