<?php

  $urlubic = "";
  require('func.includes/seguridad.php');
  include_once("func.includes/config.inc.php");

  //Recuperamos OP
  $op = secureParamToSql($_GET['op']);
  
  if($op == 'alta/de/usuarios') {
    $action   = 'create';
    $title    = 'Nuevo usuario';
  } else if($op == 'edicion/de/usuarios') {
    $action    = 'update';
    $title     = 'Editar usuario';
    $aUsuario = $oDB->usuario[secureParamToSql($_GET['id'])]; 
  }

  include_once("result_message.php");
  
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?=_global_metaDesc?>">
    <meta name="keywords" content="<?=_global_metaKeys?>">
    <meta name="author" content="<?=_global_metaAuth?>">
    <link rel="icon" href="../img/favicon.ico">

    <title><?=$title?></title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    
    <!-- Autocomplete -->
    <link rel="stylesheet" href="js/EasyAutocomplete-1.3.5/easy-autocomplete.min.css"> 
    
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
    
  </head>

  <body>

    <?php include('menu.php'); ?>

    <div class="container-fluid">

      <div class="row">

        <?php include('left_nav.php'); ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="proceso.php?op=panel/administracion">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Usuarios</li>
            </ol>
          </nav>

          <h2>
            <?=$title?> 
            <a href="proceso.php?op=panel/usuarios" class="btn btn-secondary btn-sm float-right">Volver</a>
          </h2>

          <div>
            <?=isset($result)?$result:''?>
            <form action="frm-usuarios/procesar.php?action=<?=$action?>" class="form row" method="POST" enctype="multipart/form-data" role="form">
          
              <input type="hidden" name="idOwner" class="idOwner" value="<?=$_SESSION['id']?>" />
              <?php if(isset($aUsuario)){ ?>
                <input type="hidden" name="id" id="id" value="<?=$aUsuario['id']?>" />
                <!-- <input type="hidden" name="passwordOld" id="passwordOld" value="<?=$aUsuario['password']?>" />
                <input type="hidden" name="imagenOld" id="imagenOld" value="<?=$aUsuario['imagen']?>" /> -->
              <?php } ?>

              <div class="col-8">

              <div class="form-group required">
                <label for="nombre" class="control-label">Nombre</label>
                <input type="text" name="nombre" class="form-control" id="nombre" value="<?=isset($_POST['nombre'])?$_POST['nombre']:isset($aUsuario['nombre'])?$aUsuario['nombre']:false?>" placeholder="Ingrese su nombre" required autofocus>
              </div>

              <div class="form-group required">
                <label for="apellido" class="control-label">Apellido</label>
                <input type="text" name="apellido" class="form-control" id="apellido" value="<?=isset($_POST['apellido'])?$_POST['apellido']:isset($aUsuario['apellido'])?$aUsuario['apellido']:false?>" placeholder="Ingrese su apellido" required >
              </div>

              <div class="form-group required">
                <label for="email" class="control-label">Email</label>
                <input type="email" name="email" class="form-control" id="email" value="<?=isset($_POST['email'])?$_POST['email']:isset($aUsuario['email'])?$aUsuario['email']:false?>" placeholder="ejemplo@dominio.com" required autocomplete="off">
              </div>

              <div class="form-group required">
                <label for="pais" class="control-label">País</label>
                <?php 
                  $aPaises = $oDB->tbpaises(); 
                  if($aPaises){
                ?>
                <select name="idPais" id="pais" class="form-control" required >
                  <option>Seleccione un país</option>
                  <?php foreach($aPaises as $aPais){ ?>
                  <option value="<?=$aPais['idPais']?>" <?=(isset($_POST['idPais'])&&$_POST['idPais']==$aPais['idPais'])||(isset($aUsuario['idPais'])&&$aUsuario['idPais']==$aPais['idPais'])?'selected':''?> ><?=$aPais['pais']?></option>
                  <?php } ?>
                </select>
                <?php } ?>            
              </div>

              <div class="form-group required">
                <label for="provincia" class="control-label">Provincia</label>
                <?php 
                  $aProvincias = isset($aUsuario['idPais'])&&$aUsuario['idPais']!=''?$oDB->tbprovincias()->where("idPais = ?", $aUsuario['idPais']):$oDB->tbprovincias();
                  if($aProvincias){
                ?>
                <select name="idProvincia" id="provincia" class="form-control" required>
                  <option>Seleccione una provincia</option>
                  <?php foreach($aProvincias as $aProvincia){ ?>
                  <option value="<?=$aProvincia['idProvincia']?>" <?=(isset($_POST['idProvincia'])&&$_POST['idProvincia']==$aProvincia['idProvincia'])||(isset($aUsuario['idProvincia'])&&$aUsuario['idProvincia']==$aProvincia['idProvincia'])?'selected':''?> ><?=$aProvincia['provincia']?></option>
                  <?php } ?>
                </select>
                <?php } ?>
              </div>
              
              <div class="form-group required">
                <label for="localidad" class="control-label">Localidad</label>
                <?php 
                  $aLocalidades = isset($aUsuario['idProvincia'])&&$aUsuario['idProvincia']!=''?$oDB->tblocalidades()->where("idProvincia = ?", $aUsuario['idProvincia']):$oDB->tblocalidades(); 
                  if($aLocalidades){
                ?>
                <select name="idLocalidad" id="localidad" class="form-control" required>
                  <option>Seleccione una localidad</option>
                  <?php foreach($aLocalidades as $aLocalidad){ ?>
                  <option value="<?=$aLocalidad['idLocalidad']?>" <?=(isset($_POST['idLocalidad'])&&$_POST['idLocalidad']==$aLocalidad['idLocalidad'])||(isset($aUsuario['idLocalidad'])&&$aUsuario['idLocalidad']==$aLocalidad['idLocalidad'])?'selected':''?> ><?=$aLocalidad['localidad']?></option>
                  <?php } ?>
                </select>
                <?php } ?>
              </div>

              <div class="form-group required">
                <label for="userName" class="control-label">Nombre de usuario</label>
                <input type="text" name="userName" class="form-control" id="userName" value="<?=isset($_POST['userName'])?$_POST['userName']:isset($aUsuario['userName'])?$aUsuario['userName']:false?>" required >
              </div>

              <div class="form-group">
                <label for="password" class="control-label">Password</label>
                <input type="password" name="password" class="form-control" id="password" value="<?=isset($_POST['password'])?$_POST['password']:false?>" >
              </div>
              
              <div class="form-group required">
                <label for="rol_id" class="control-label">Rol</label>
                <?php 
                  $aRoles = $oDB->roles();
                  if($aRoles){
                ?>
                <select class="form-control input-sm" id="rol_id" name="rol_id" required>
                  <option value="">Seleccione</option>
                  <?php foreach ($aRoles as $aRol) { ?>
                  <option value="<?=$aRol['id']?>" <?=(isset($_POST['rol_id'])&&$_POST['rol_id']==$aRol['id'])||(isset($aUsuario['rol_id'])&&$aUsuario['rol_id']==$aRol['id'])?'selected="selected"':''?>><?=$aRol['nombre']?></option>
                  <?php } ?>
                </select>
                <?php } ?>
              </div>

            </div> <!-- Fin col-8 -->

            <sidebar class="col-4">

              <div class="card border-primary mb-3">
                <div class="card-header">
                  <i class="fas fa-save"></i> Publicar
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <label for="confirmado" class="control-label"><i class="fa fa-eye"></i> Estado</label><br>
                    <input type="radio" name="confirmado" value="SI" <?=(isset($_POST['confirmado'])&&$_POST['confirmado']=='SI')||(isset($aUsuario['confirmado'])&&$aUsuario['confirmado']=='SI')||$action=='create'?'checked':''?> > Confirmado
                    <input type="radio" name="confirmado" value="NO" <?=(isset($_POST['confirmado'])&&$_POST['confirmado']=='NO')||(isset($aUsuario['confirmado'])&&$aUsuario['confirmado']=='NO')?'checked':''?> > Pendiente
                  </div>
                  <?php if($action=='update'){ ?>
                  <p>
                    <i class="fas fa-calendar-alt"></i> Ingresado el: <b><?php echo fecha_completa($aUsuario['add_date']); ?></b>
                  </p>
                  <?php } ?>
                </div>
                <div class="card-footer text-right">
                  <?php if($action == 'update'){ ?>
                    <a href="#" class="delete-row float-left" data-id="<?=$aUsuario['id']?>" alt="<?=$aUsuario['nombre']?>" title="Eliminar" style="margin-top:6px;"><i class="fa fa-trash"></i> Eliminar</a>
                  <?php } ?>
                  <input name="procesar" type="submit" id="procesar" value="Guardar" class="btn btn-primary btn-sm" />
                </div>
              </div>

              <div class="card border-primary">
                <div class="card-header">
                  <i class="fas fa-image"></i> &nbsp;Imagen
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <label for="avatar" class="custom-file"> 
                      <input type="file" name="avatar" id="avatar" value="<?=isset($_POST['avatar'])?$_POST['avatar']:isset($aUsuario['avatar'])?$aUsuario['avatar']:false?>" class="custom-file-input" />
                      <span class="custom-file-control">Seleccionar avatar...</span>
                    </label>
                    <small class="help-block">Campo requerido. <br>590x415px es el tamaño sugerido para una mejor visualización en el sitio.</small>
                  </div>
                  <!-- <div class="form-group">
                    <input type="text" name="alt" class="form-control" id="alt" value="<?=isset($_POST['alt'])?$_POST['alt']:isset($aUsuario['alt'])?$aUsuario['alt']:false?>" placeholder="Texto alternativo de la imagen">
                  </div> -->
                </div>
                <?php if(isset($aUsuario['avatar'])&&$aUsuario['avatar']!=''){ ?>
                  <img src="<?=_global_userurl.$aUsuario['avatar']?>" class="card-img-bottom">
                <?php } ?>
              </div>

            </sidebar> <!-- Fin sidebar -->

          </form>

        </div>

      </main>

    </div>

  </div> <!-- /container -->

  <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true" id="delete-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Se necesita confirmación</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-sm" id="modal-btn-si">Si</button>
          <button type="button" class="btn btn-secondary btn-sm" id="modal-btn-no" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
  
  <?php include('footer.php'); ?>

    <!-- Otras funciones -->
    <script type="text/javascript">
      $(document).ready(function() {
        //Boton eliminar
        $('.card-footer').on( 'click', 'a.delete-row', function(event) {
          var id      = $(this).attr('data-id');
          var idOwner = $('#idOwner').text();
          var nombre  = $(this).attr('alt');
          var url     = 'frm-usuarios/procesar.php?action=delete';
          var options = {
            backdrop: 'static'
          };

          $('#delete-modal .modal-body').html("Esta seguro que desea eliminar a <b>" + nombre + "</b>?");
          $('#delete-modal').modal(options);
          $("#modal-btn-si").on("click", function(){
            $.ajax({
              url: url,
              type: 'POST',
              dataType: 'json',
              data: { id: id, idOwner: idOwner },
              success: function(data){
                //alert(JSON.stringify(data, null, 4));
                if(data.success){
                  $(location).attr('href', 'proceso.php?op=panel/usuarios');                
                }
              },
              error: function(data){
                alert('Ocurrio un error eliminando el registro');
              }
            });
          
            event.preventDefault();
          });
        });
        
        //Get provincias
        $("#pais").on('change', function(){
          var id = $(this).find(":selected").val();
          var url = 'frm-usuarios/procesar.php';
          var dataString = 'idPais='+ id +'&action=getProv';
          var idProvincia;
          var provincia;

          $('#provincia').empty().append('<option></option>');
          $('#localidad').empty().append('<option></option>');

          $.ajax({                                      
            url: url,                                            
            data: dataString,   // tambien puede ser: data: { action: "getProv", idPais : id },                                 
            dataType: 'json', 
            beforeSend: function(url)
            {
              //alert(JSON.stringify(dataString, null, 4));
              $('#bgloading').fadeIn();
            },                    
            success: function(data)          
            {
              $(data).each(function( key,value ) {
                //console.log(JSON.stringify(data, null, 4));
                idProvincia = data[key]['idProvincia'];
                provincia = data[key]['provincia'];
                $('#provincia').append('<option value="'+ idProvincia +'">'+ provincia +'</option>');
              });
              $('#bgloading').fadeOut();   
            },
            error: function(result) {
              alert("No se encontraron datos para la opción seleccionada.");
              $('#bgloading').fadeOut();
            }
          });
        });
        
        //Get localidad
        $("#provincia").on('change', function(){
          var id = $(this).find(":selected").val();
          var url = 'frm-usuarios/procesar.php';
          var dataString = 'idProvincia='+ id +'&action=getLoc';
          var idLocalidad;
          var localidad;
          $('#localidad').empty().append('<option></option>');
          $.ajax({                                      
            url: url,                                            
            data: dataString,   // tambien puede ser: data: { action: "getLoc", idProvincia : id },                                 
            dataType: 'json', 
            beforeSend: function(url)
            {
              $('#bgloading').fadeIn();
              //alert(JSON.stringify(dataString, null, 4));
            },                    
            success: function(data)          
            {    
              $(data).each(function( key,value ) {
                //console.log(JSON.stringify(data, null, 4));
                idLocalidad = data[key]['idLocalidad'];
                localidad = data[key]['localidad'];
                $('#localidad').append('<option value="'+ idLocalidad +'">'+ localidad +'</option>');
              });
              $('#bgloading').fadeOut();
            },
            error: function(result) {
              alert("No se encontraron datos para la opción seleccionada.");
              $('#bgloading').fadeOut();
            }
          });

        });

      });
    </script>

  </body>
</html>
