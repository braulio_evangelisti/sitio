<?php

try
{

	$urlubic = "../";
	include_once($urlubic."func.includes/config.inc.php");

	/* 
	* Definir tabla y clave
	*/
	$sTable = "usuario";
	$sId 	= "id";

	//Nuevo registro
	if($_GET["action"] == "create"){
		$aData = [
			'slug' => to_prety_url($_POST['userName']),
			'nombre' => $_POST['nombre'],
			'apellido' => $_POST['apellido'],
			'email' => $_POST['email'],
			'idPais' => $_POST['idPais'],
			'idProvincia' => $_POST['idProvincia'],
			'idLocalidad' => $_POST['idLocalidad'],
			'userName' => $_POST['userName'],
			'rol_id' => $_POST['rol_id'],
			'confirmado' => $_POST['confirmado'],
			'add_user' => $_POST['idOwner'],
			'add_date' => date("Y-m-d H:m:s"),
			'eliminado' => 0
		];
		//+ Extra fields
		if(isset($_POST['password'])&&$_POST['password']!='')
			$adata['password'] = sha1($_POST['password']);

		if($_FILES['avatar']['name']!=""){
			$imagen = time()."_".$_FILES['avatar']['name'];
			$destino = _global_userpath.$imagen ;
			move_uploaded_file($_FILES['avatar']['tmp_name'],$destino);
			$aData['avatar'] = $imagen;
		}

		if(isset($_POST['alt'])&&$_POST['alt']!='')
			$adata['alt'] = $_POST['alt'];

		//Insert record into database
		$ins = $oDB->usuario()->insert($aData);
		
		if(isset($ins['id']))
			header('location: ../proceso.php?op=alta/de/usuarios&result=ok');
		else
			header('location: ../proceso.php?op=alta/de/usuarios&result=bad');
	}
	//Editar registro
	else if($_GET["action"] == "update"){
		//Get ID
		$oRegistro = $oDB->usuario[secureParamToSql($_POST[$sId])]; 
		
		if($oRegistro){
			$aData = [
				'slug' => to_prety_url($_POST['userName']),
				'nombre' => $_POST['nombre'],
				'apellido' => $_POST['apellido'],
				'email' => $_POST['email'],
				'idPais' => $_POST['idPais'],
				'idProvincia' => $_POST['idProvincia'],
				'idLocalidad' => $_POST['idLocalidad'],
				'userName' => $_POST['userName'],
				'rol_id' => $_POST['rol_id'],
				'confirmado' => $_POST['confirmado'],
				'edit_user' => $_POST['idOwner'],
				'edit_date' => date("Y-m-d H:m:s"),
			];

			// Extra fields
			if(isset($_POST['password'])&&$_POST['password']!='')
				$adata['password'] = sha1($_POST['password']);

			if($_FILES['avatar']['name']!=""){
				$imagen = time()."_".$_FILES['avatar']['name'];
				$destino =  _global_userpath.$imagen;
				move_uploaded_file($_FILES['avatar']['tmp_name'],$destino);
				$aData['avatar'] = $imagen;
			}

			if(isset($_POST['alt'])&&$_POST['alt']!='')
				$adata['alt'] = $_POST['alt'];

			//Update record in database
			$upd = $oRegistro->update($aData);
		}

		if($upd)
			header('location: ../proceso.php?op=panel/usuarios&result=ok');
		else
			header('location: ../proceso.php?op=panel/usuarios&result=bad');
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete"){
		//Defino el array donde voy a mandar los resultados de vuelta
		$aResult = [];

		//Get ID
		$oRegistro = $oDB->usuario[secureParamToSql($_POST['id'])];
		if($oRegistro){
			$aData = [
				'kill_user' => $_POST['idOwner'],		
				'kill_date' => date("Y-m-d H:m:s"),
				'eliminado' => 1
			];
			//"Delete" from database
			$aResult['success'] = $oRegistro->update($aData);
		} else {
			$aResult['success'] = false;
		}

		//Return data para comprobar si se elimino ya que se hace con AJAX necesitamos json_encode
		print(json_encode($aResult));
		
	}
	//getProvincia
	else if($_GET["action"]=="getProv"){
		$aProvincias = $oDB->tbprovincias->where("idPais = ?",secureParamToSql($_GET['idPais']));
        if($aProvincias)
	        echo json_encode($aProvincias);
	}
	//getLocalidad
	else if($_GET["action"]=="getLoc"){
		$aLocalidades = $oDB->tblocalidades->where("idProvincia = ?",secureParamToSql($_GET['idProvincia']));
        if($aLocalidades)
	        echo json_encode($aLocalidades);
	}

}
catch(Exception $ex)
{
    //Return error message
	$result = [];
	$result['Result'] = "ERROR";
	$result['Message'] = $ex->getMessage();
	print_r(json_encode($result));
}
	
?>