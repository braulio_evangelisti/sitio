<?php
  $urlubic = "../";
  require($urlubic.'func.includes/seguridad.php');
  include_once($urlubic."func.includes/config.inc.php");

  $aReturn = []; 
  $aClases = [
    'slider' => 'info',
    'empresa' => 'dark',
    'novedades' => 'primary',
    'productos' => 'danger',
    'servicios' => 'warning',
    'sucursales' => 'dark'
  ];

  $aTemps['slider'] = $oDB->slider()
                    ->select("id, titulo")
                    ->where("titulo LIKE ?", "%".secureParamToSql($_GET['q'])."%")
                    ->and("eliminado = ?", 0);
  
  $aTemps['empresa'] = $oDB->empresa()
                    ->select("id, titulo")
                    ->where("titulo LIKE ?", "%".secureParamToSql($_GET['q'])."%")
                    ->and("eliminado = ?", 0);

  $aTemps['novedades'] = $oDB->novedad()
                    ->select("id, titulo")
                    ->where("titulo LIKE ?", "%".secureParamToSql($_GET['q'])."%")
                    ->and("eliminado = ?", 0);

  $aTemps['productos'] = $oDB->producto()
                    ->select("id, titulo")
                    ->where("titulo LIKE ?", "%".secureParamToSql($_GET['q'])."%")
                    ->and("eliminado = ?", 0);

  $aTemps['servicios'] = $oDB->servicio()
                    ->select("id, titulo")
                    ->where("titulo LIKE ?", "%".secureParamToSql($_GET['q'])."%")
                    ->and("eliminado = ?", 0);

  $aTemps['sucursales'] = $oDB->sucursal()
                    ->select("id, titulo")
                    ->where("titulo LIKE ?", "%".secureParamToSql($_GET['q'])."%")
                    ->and("eliminado = ?", 0);

  foreach($aTemps as $sTipo=>$aTemp){
    foreach ($aTemp as $aRegistro) {
      $aReturn[] = [
        'id' => $aRegistro['id'],
        'titulo' => $aRegistro['titulo'],
        'descripcion' => "<i class='badge badge-".$aClases[$sTipo]."'>" . ucfirst($sTipo) ."</i> ",
        'url' => "proceso.php?op=edicion/de/".$sTipo."&id=".$aRegistro['id']
      ];
    }
  }

  if($_GET['format']==='json'){
    header('Content-Type: application/json');
    echo json_encode($aReturn);
  }
?>