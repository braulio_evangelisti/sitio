<?php
      
  $urlubic = "";
  require($urlubic.'func.includes/seguridad.php');
  include_once($urlubic."func.includes/config.inc.php");
  $aLastNew = $oDB->novedad()->where("eliminado = ?", 0)->order('add_date DESC')->limit(1)->fetch();
  $aLastProd = $oDB->producto()->where("eliminado = ?", 0)->order('add_date DESC')->limit(1)->fetch();
  $aLastServ = $oDB->servicio()->where("eliminado = ?", 0)->order('add_date DESC')->limit(1)->fetch();
  $aLastEmp = $oDB->empresa()->where("eliminado = ?", 0)->order('add_date DESC')->limit(1)->fetch();
  $aLastUsers = $oDB->usuario()->where("eliminado = ?", 0)->order('add_date DESC')->limit(5);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?=_global_metaDesc?>">
    <meta name="keywords" content="<?=_global_metaKeys?>">
    <meta name="author" content="<?=_global_metaAuth?>">
    <link rel="icon" href="../img/favicon.ico">

    <title><?=_global_panelname?></title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    
    <!-- Autocomplete -->
    <link rel="stylesheet" href="js/EasyAutocomplete-1.3.5/easy-autocomplete.min.css"> 
    
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
  </head>

  <body>

    <?php include('menu.php'); ?>

    <div class="container-fluid">
      <div class="row">

        <?php include('left_nav.php'); ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          
          <div class="row pb-2">
            <div class="col-12">
              <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <h1 class="h2">Escritorio</h1>
                <!-- <div class="btn-toolbar mb-2 mb-md-0">
                  <div class="btn-group mr-2">
                    <button class="btn btn-sm btn-outline-secondary">Share</button>
                    <button class="btn btn-sm btn-outline-secondary">Export</button>
                  </div>
                  <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                    <span data-feather="calendar"></span>
                    This week
                  </button>
                </div> -->
              </div>
              <div class="card-deck">
                
                <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                  <div class="card-header">
                    <i class="fas fa-newspaper"></i>
                    Novedades 
                    <span class="badge badge-light float-right p-2">
                      <?=count($oDB->novedad())?>
                    </span>
                  </div>
                  <div class="card-body">
                    <h5 class="card-title"><?=$aLastNew['titulo']?></h5>
                    <p class="card-text">
                      <?=$aLastNew['bajada']==''?extracto($aLastNew['descripcion'], 5):extracto($aLastNew['bajada'], 5)?>
                    </p>
                  </div>
                  <div class="card-footer">
                    <label class="mt-1">
                      <i class="fas fa-clock"></i>
                      <?=fecha_completa_abreviada($aLastNew['add_date'])?>
                    </label>
                    <div class="btn-toolbar float-right">
                      <div class="btn-group">
                        <a href="proceso.php?op=alta/de/novedades" class="btn btn-sm btn-outline-light">
                          <i class="fas fa-plus"></i>
                        </a>
                        <a href="proceso.php?op=edicion/de/novedades&amp;&id=<?=$aLastNew['id']?>" class="btn btn-sm btn-outline-light">
                          <i class="fas fa-pencil-alt"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card text-white bg-danger mb-3" style="max-width: 18rem;">
                  <div class="card-header">
                    <i class="fas fa-shopping-basket"></i>
                    Productos 
                    <span class="badge badge-light float-right p-2">
                      <?=count($oDB->producto())?>
                    </span>
                  </div>
                  <div class="card-body">
                    <h5 class="card-title"><?=$aLastProd['titulo']?></h5>
                    <p class="card-text">
                      <?=$aLastProd['bajada']==''?extracto($aLastProd['descripcion'], 5):extracto($aLastProd['bajada'], 5)?>
                    </p>
                  </div>
                  <div class="card-footer">
                    <label class="mt-1">
                      <i class="fas fa-clock"></i>
                      <?=fecha_completa_abreviada($aLastProd['add_date'])?>
                    </label>
                    <div class="btn-toolbar float-right">
                      <div class="btn-group">
                        <a href="proceso.php?op=alta/de/productos" class="btn btn-sm btn-outline-light">
                          <i class="fas fa-plus"></i>
                        </a>
                        <a href="proceso.php?op=edicion/de/productos&amp;&id=<?=$aLastProd['id']?>" class="btn btn-sm btn-outline-light">
                          <i class="fas fa-pencil-alt"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card text-white bg-warning mb-3" style="max-width: 18rem;">
                  <div class="card-header">
                    <i class="fas fa-tasks"></i>
                    Servicios 
                    <span class="badge badge-light float-right p-2">
                      <?=count($oDB->servicio())?>
                    </span>
                  </div>
                  <div class="card-body">
                    <h5 class="card-title"><?=$aLastServ['titulo']?></h5>
                    <p class="card-text">
                      <?=$aLastServ['bajada']==''?extracto($aLastServ['descripcion'], 5):extracto($aLastServ['bajada'], 5)?>
                    </p>
                  </div>
                  <div class="card-footer">
                    <label class="mt-1">
                      <i class="fas fa-clock"></i>
                      <?=fecha_completa_abreviada($aLastServ['add_date'])?>
                    </label>
                    <div class="btn-toolbar float-right">
                      <div class="btn-group">
                        <a href="proceso.php?op=alta/de/servicios" class="btn btn-sm btn-outline-light">
                          <i class="fas fa-plus"></i>
                        </a>
                        <a href="proceso.php?op=edicion/de/servicios&amp;&id=<?=$aLastServ['id']?>" class="btn btn-sm btn-outline-light">
                          <i class="fas fa-pencil-alt"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                  <div class="card-header">
                    <i class="fas fa-industry"></i>
                    Empresa 
                    <span class="badge badge-light float-right p-2">
                      <?=count($oDB->empresa())?>
                    </span>
                  </div>
                  <div class="card-body">
                    <h5 class="card-title"><?=$aLastEmp['titulo']?></h5>
                    <p class="card-text">
                      <?=$aLastEmp['bajada']==''?extracto($aLastEmp['descripcion'], 5):extracto($aLastEmp['bajada'], 5)?>
                    </p>
                  </div>
                  <div class="card-footer">
                    <label class="mt-1">
                      <i class="fas fa-clock"></i>
                      <?=fecha_completa_abreviada($aLastEmp['add_date'])?>
                    </label>
                    <div class="btn-toolbar float-right">
                      <div class="btn-group">
                        <a href="proceso.php?op=alta/de/empresa" class="btn btn-sm btn-outline-light">
                          <i class="fas fa-plus"></i>
                        </a>
                        <a href="proceso.php?op=edicion/de/empresa&amp;&id=<?=$aLastEmp['id']?>" class="btn btn-sm btn-outline-light">
                          <i class="fas fa-pencil-alt"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
          
          <div class="row pb-2">

            <div class="widget col-6">

              <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <h2>Usuarios</h2>
                <div class="btn-toolbar mb-2 mb-md-0">
                  <a href="proceso.php?op=panel/usuarios" class="btn btn-sm btn-outline-secondary">
                    <i class="fas fa-plus"></i>
                  </a>
                </div>
              </div>

              <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Email</th>
                  <th>Contacto</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($aLastUsers as $aUser){ ?>
                  <tr>
                    <td><?=$aUser['id']?></td>
                    <td><?=$aUser['nombre'].' '.$aUser['apellido']?></td>
                    <td><?=$aUser['email']?></td>
                    <td><?=$aUser['telefono']?></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
              
            </div>

            <?php 
              $aSlider = $oDB->slider()->where("eliminado = ?", 0)->order('orden ASC');
              if($aSlider){
            ?>
            <div class="widget col-6">

              <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <h2>Slider</h2>
                <div class="btn-toolbar mb-2 mb-md-0">
                  <a href="proceso.php?op=panel/slider" class="btn btn-sm btn-outline-secondary">
                    <i class="fas fa-pencil-alt"></i>
                  </a>
                </div>
              </div>

              <div id="carouselHome" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <?php foreach($aSlider as $k => $aSlide){ ?>
                  <li data-target="#carouselHome" data-slide-to="<?=$k-1?>" class="<?=$k==1?'active':''?>"></li>
                  <?php } ?>
                </ol>
                <div class="carousel-inner">
                <?php foreach($aSlider as $k => $aSlide){ ?>
                  <div class="carousel-item <?=$k==1?'active':''?>">
                    <img class="d-block w-100" src="<?=_global_sliderurl.$aSlide['imagen']?>" alt="<?=$aSlide['alt']?>" style="height: 225px;max-height: 225px;">
                  </div>
                <?php } ?>
                </div>
                <a class="carousel-control-prev" href="#carouselHome" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselHome" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>

            </div>
            <?php } ?>
          </div>
          <!-- <div class="jumbotron">
            <h1>Bienvenido, <?=$_SESSION['USERNAME']?></h1>
            <p>Este es el escritorio de su panel de control.</p>
            <p>Entre sus características principales se encuentran:</p>
            <p>
              <dl>
                <dt>Gestionar usuarios</dt>
                <dd><small>Gestión de los usuarios ingresados al sistema.</small></dd>
                <p></p>
                <dt>Administrar permisos</dt>
                <dd><small>Administración de permisos concedidos a usuarios según nivel de contenido.</small></dd>
                <p></p>  
                <dt>Actualizar contenido</dt>
                <dd><small>Crear y actualizar el contenido del sitio.</small></dd>
              </dl>
            </p>
              
            </p>
          </div> -->
        </main>
      </div>

    </div> <!-- /container -->
    
    <?php include('footer.php'); ?>
  </body>
</html>
