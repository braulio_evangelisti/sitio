<?php

	include_once("func.includes/class_login.php");
	include_once("func.includes/config.inc.php");
	
	if (isset($_POST["procesar"])){

		/* NECESARIO RESPONSE CAPTCHA */
		$reCaptcha = new \ReCaptcha\ReCaptcha(_privatekey);
		$oResponse = $reCaptcha->setExpectedHostname($_SERVER['SERVER_NAME'])
		 						->verify($_POST["g-recaptcha-response"], $_SERVER["REMOTE_ADDR"]);

		if (empty($_POST["g-recaptcha-response"])){
			/* Si el captcha está vacío */
			header("Location: index.php?estado=5");
			exit();
		} else if(!$oResponse->isSuccess()){
			/* Si el captcha es incorrecto */ 
			header("Location: index.php?estado=5");
			exit();
		} else {
			/* Si el captcha es correcto */	

			$oLogin = new Login();
			$oLogin->setCryptMethod('sha1');

			$email 		= secureParamToSql($_POST['email']);
			$password 	= $oLogin->setCrypt($_POST['password']);

			$aUser 		= $oDB->usuario("email = ?", $email)->fetch();

			if($aUser['id']){

				$db_password = $aUser['password'];

				if ($password == $db_password){

					session_start();

					$_SESSION['id']					= $aUser['id'];
					$_SESSION['user_login_session']	= true;
					$_SESSION['USERNAME']       	= $USERNAME	= $aUser['userName'];
					$_SESSION['ADMIN_PW']       	= $ADMIN_PW = $aUser['password'];
					$_SESSION['USER']  	        	= $USER		= $aUser['apellido']." ".$aUser['nombre'];
					$_SESSION['AVATAR'] 	  		= $aUser['imagen'];
					$_SESSION['NIVEL'] 	  			= $aUser['id_rol'];

					header("Location: proceso.php?op=panel/administracion");
					exit();

				} else {
					header("Location: index.php?estado=2");
					exit();
				}
			} else {
				header("Location: index.php?estado=1");
				exit();
			}

		} /*Captcha*/

	} /*Procesar*/
?>