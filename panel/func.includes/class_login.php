<?php

/**
 * Project:     Login PHP Class
 * File:        class_login.php
 * Purpose:     Authenticates users by checking their data in a mysql database
 *
 * For questions, help, comments, discussion, etc, please send
 * e-mail to antonio.desire@gmail.com
 *
 * @link http://antoniociccia.netsons.org
 * @author Antonio Ciccia <antonio.desire@gmail.com>
 * @package Login PHP Class
 * @version 1.0
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

class Login {

    /**
     * Crypting method: md5|sha1
     * 
     * @var string
     */
    private $cryptMethod;

    /**
     * Sets the crypting method
     *
     * @param string $crypt_method - You can set it as 'md5' or 'sha1' to choose the crypting method for the user password.
     */
    public function setCryptMethod($crypt_method){
        $this->cryptMethod=$crypt_method;
    }

    /**
     * Crypts a string
     *
     * @param string $text_to_crypt - crypt a string if $this->cryptMethod was defined.
     * If not, the string will be returned uncrypted.
     */
    public function setCrypt($text_to_crypt){
        switch($this->cryptMethod){
            case 'md5': $text_to_crypt=trim(md5($text_to_crypt)); break;
            case 'sha1': $text_to_crypt=trim(sha1($text_to_crypt)); break;
        }
       return $text_to_crypt;
    }

    /**
     * Anti-Mysql-Injection method, escapes a string.
     * 
     * @param string $text_to_escape
     * @param PDO    $oConnection   
     */
    static public function setEscape($text_to_escape, $oConnection){
        return $oConnection->quote($text_to_escape);
    }

}

?>