<?php

	//print_r($_POST);
	//die();

	include_once("func.includes/config.inc.php");
	// include_once("func.includes/utiles.inc.php");

	if (isset($_POST["procesar"])){

		$reCaptcha = new \ReCaptcha\ReCaptcha(_privatekey);
		$oResponse = $reCaptcha->setExpectedHostname($_SERVER['SERVER_NAME'])
		 						->verify($_POST["g-recaptcha-response"], $_SERVER["REMOTE_ADDR"]);

		if (empty($_POST["g-recaptcha-response"])){
			/* Si el captcha está vacío */
			header("Location: index.php?estado=5");
			exit();
		} else if(!$oResponse->isSuccess()){
			/* Si el captcha es incorrecto */ 
			header("Location: index.php?estado=5");
			exit();
		} else {
			/* Si el captcha es correcto */	

			$email 	= secureParamToSql($_POST['email']);

			$aUser 	= $oDB->usuario("email = ?, eliminado = ?", $email, 0)->fetch();

			if (!isset($aUser['id'])) 
	        {
	            header("Location: index.php?estado=1");
				exit();
	        }
	        else
	        {

	        	$encrypt = sha1(90*13+$aUser['id']);
				$cuerpo  ='Hola, <br/> <br/>Su id de cuenta es: '.$aUser['id'].' <br><br>Click <a href="'. _global_siteurl .'adm-gestor/proceso.php?op=reset&encrypt=' . $encrypt . '">aquí</a> para resetear su password';
				try {
				    // Server settings
					$mail = new PHPMailer();						// Passing `true` enables exceptions
					// $mail->SMTPDebug = 2;                        // Enable verbose debug output
					$mail->isSMTP(); 								// Set mailer to use SMTP
					$mail->Host = _global_stmp;						// Specify main and backup SMTP servers
					$mail->SMTPAuth = true;							// Enable SMTP authentication
					$mail->Username = _global_mail_acc;				// SMTP username
				    $mail->Password = _global_mail_pass; 			// SMTP password
				    $mail->SMTPSecure = 'tls';                      // Enable TLS encryption, `ssl` also accepted
	    			$mail->Port = 587;                              // TCP port to connect to
	    			// To load the French version
					$mail->setLanguage('es', 'composer_vendor/phpmailer/phpmailer/language/');
				    
				    //Recipients
				    $mail->setFrom('recovery@sitio.com.ar', 'Administrador del sitio');
				    // $mail->addAddress('joe@example.net', 'Joe User');    // Add a recipient
				    $mail->addAddress($email);               				// Name is optional
				    $mail->addReplyTo('info@sitio.com.ar', 'Information');
				    // $mail->addCC('cc@example.com');
				    // $mail->addBCC('bcc@example.com');
				    
				    //Attachments
				    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
				    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

				    //Content
				    $mail->IsHTML(true); 							// Set email format to HTML
				    $mail->CharSet = "UTF-8";
				    $mail->Subject = "Forget Password";
				    $mail->Body = $cuerpo;
				    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
				    
				    if($mail->Send()){
				      header("Location: index.php?estado=6");
					  exit();
				    } else {
				      header("Location: index.php?estado=7");
					  exit();
				    }
				} catch (Exception $e) {
				    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
				}

			} /*row*/

		}

	} /*Procesar*/
?>