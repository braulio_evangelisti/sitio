<?php
	
	//Define host para ambos, local y web
	$host = "localhost";
	
	//Setea datos de conexion locales/online, segun donde estemos trabajando 
	if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1' || $_SERVER['REMOTE_ADDR'] == '::1') {
		$basedato = "panel_db"; 
		$user 	  = "root";
		$pass 	  = "r00t";
		define('_global_siteurl'	,'http://localhost/sitio/');
	} else {
		$basedato = "gaaagnia_sitio2018"; 
		$user 	  = "gaaagnia_user";
		$pass 	  = "I~g1(8}gPP8=";
		define('_global_siteurl'	,'http://69.61.11.250/~gaaagnia28112017/');
		//define('_global_siteurl'	,'http://gravasargentinas.com.ar/');

	}

	// panel data
	define('_global_panelname'	,'Panel Administrativo');

	// Paths Generales
	define('_global_panelurl'	,_global_siteurl.'panel/');	
	define('_global_app_dir'	,dirname(dirname(dirname(__FILE__)))); // application root directory
	define('_func_includes'		,_global_panelurl.'func.includes/');

	// Imagenes
	define('_global_imgurl'		,_global_siteurl.'images/');	
	define('_global_imgpath'	,_global_app_dir.'/images/');
	define('_global_sliderurl'	,_global_imgurl.'slider/');	
	define('_global_sliderpath'	,_global_imgpath.'slider/');
	define('_global_caturl'		,_global_imgurl.'categorias/');	
	define('_global_catpath'	,_global_imgpath.'categorias/');
	define('_global_userurl'	,_global_imgurl.'usuarios/');	
	define('_global_userpath'	,_global_imgpath.'usuarios/');
	define('_global_clienturl'	,_global_imgurl.'clientes/');	
	define('_global_clientpath'	,_global_imgpath.'clientes/');
	define('_global_newsurl'	,_global_imgurl.'noticias/');	
	define('_global_newspath'	,_global_imgpath.'noticias/');
	define('_global_insturl'	,_global_imgurl.'empresa/');	
	define('_global_instpath'	,_global_imgpath.'empresa/');
	define('_global_servurl'	,_global_imgurl.'servicios/');	
	define('_global_servpath'	,_global_imgpath.'servicios/');
	define('_global_produrl'	,_global_imgurl.'productos/');	
	define('_global_prodpath'	,_global_imgpath.'productos/');
	define('_global_porturl'	,_global_imgurl.'portfolio/');	
	define('_global_portpath'	,_global_imgpath.'portfolio/');

	// Files
	define('_global_fileurl'	,_global_siteurl.'files/');	
	define('_global_filepath'	,_global_app_dir.'/files/');

	// Valores constantes globales
	define('_global_mail_info'  ,'info@sitio.com.ar');
	define('_global_stmp'	    ,'smtp.'.$_SERVER['SERVER_NAME']);
	define('_global_mail_acc'	,'test@sitio.com.ar');
	define('_global_mail_pass'	,'definir pass de test@sitio.com.ar para validar el php mailer');
	
	define('_thumb_sm_sizex'	, 260);
	define('_thumb_sm_sizey'	, 160);
	define('_thumb_md_sizex'	, 480);
	define('_thumb_md_sizey'	, 0);
	define('_thumb_lg_sizex'	, 590);
	define('_thumb_lg_sizey'	, 0);


	//No Captcha [Google private and public keys]
	define('_publickey'			,'6LcDyTsUAAAAAEwblMPK8rr_GXKZvJiD5Nkp2R3l');
	define('_privatekey'		,'6LcDyTsUAAAAADH1QPL4WeVtqtP-oKDoWo_Gb7PZ');
	
	//Valores del HEAD publico
	define('_global_metaTitle'	,'Sitio web');
	define('_global_metaAuth'	,'Author');
	define('_global_metaDesc'	,'Description');
	define('_global_metaKeys'	,'Key, Words');

?>