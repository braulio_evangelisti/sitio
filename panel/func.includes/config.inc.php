<?php

	error_reporting(E_ALL);
	//error_reporting(E_ERROR | E_WARNING | E_PARSE);
	
	include_once("utiles.inc.php");
	
	include('data.inc.php'); 

	require __DIR__."/../composer_vendor/autoload.php";

	class ParentStructure extends NotORM_Structure_Convention {
		function getReferencedTable($name, $table) {
			if ($name == "parent") {
				return $table;
			}
		}
	}

	/**
	* DB config
	*/
	$oConnection = new PDO("mysql:dbname={$basedato}", $user, $pass);
	$oDB = new NotORM($oConnection, new ParentStructure());

	// $size 	   = 512000;
	// $ext  	   = array('.jpg','.jpeg','.JPG','.JPEG','.png','.PNG');
	// $file_name = $_FILES['imagen']['name'];
	// $file_ext  = substr($file_name, strrpos($file_name,"."));
	
?>