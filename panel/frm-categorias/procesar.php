<?php

try
{

	$urlubic = "../";
	
	include_once($urlubic."func.includes/config.inc.php");

	/* 
	* Definir tabla y clave
	*/
	$sTable = "categoria";
	$sId 	= "id";

	//Nuevo registro
	if($_GET["action"] == "create"){
		$aData = [
			'nombre' => $_POST['nombre'],
			'slug' => to_prety_url($_POST['nombre']),
			'parent_id' => (isset($_POST['parent_id'])?$_POST['parent_id']:0),
			'tipo' => $_POST['tipo'],
			'publicada' => $_POST['publicada'],
			'add_user' => $_POST['idOwner'],
			'add_date' => date("Y-m-d H:m:s"),
			'eliminado' => 0
		];
		//+ Extra fields
		if($_FILES['imagen']['name']  != ""){
			$foo = new upload($_FILES['imagen']);

			if ($foo->uploaded){

				$foo->file_name_body_add   = '-large';
				$foo->image_resize         = true;
				$foo->image_x 			   = _thumb_lg_sizex;
				if(_thumb_lg_sizey===0){
   					$foo->image_ratio_y    = true;
				} else{
					$foo->image_ratio_crop = true;
   					$foo->image_y          = _thumb_lg_sizey;
				}
				$foo->Process(_global_catpath);
				$aData['imagen']		   = $foo->file_dst_name_body.'.'.$foo->file_dst_name_ext;

				$foo->file_name_body_add   = '-medium';
				$foo->image_resize         = true;
				$foo->image_x              = _thumb_md_sizex;
				if(_thumb_md_sizey===0){
					$foo->image_ratio_y	   = true;
				} else {
					$foo->image_ratio_crop = true;
   					$foo->image_y          = _thumb_lg_sizey;
				}
				$foo->Process(_global_catpath);
				$aData['imagenmd']		   = $foo->file_dst_name_body.'.'.$foo->file_dst_name_ext;

				$foo->file_name_body_add   = '-small';
				$foo->image_resize         = true;
				$foo->image_x          	   = _thumb_sm_sizex;
				if(_thumb_sm_sizey===0){
					$foo->image_ratio_y	   = true;
				} else {
					$foo->image_ratio_crop = true;
					$foo->image_y          = _thumb_sm_sizey;
				}
				$foo->Process(_global_catpath);
				$aData['imagensm']		   = $foo->file_dst_name_body.'.'.$foo->file_dst_name_ext;

				$foo->Clean();
			}
		} else {
			unset($_POST['imagen']);
		}

		if(isset($_POST['alt'])&&$_POST['alt']!='')
			$adata['alt'] = $_POST['alt'];

		//Insert record into database
		$ins = $oDB->categoria()->insert($aData);

		if(isset($ins['id']))
			header('location: ../proceso.php?op=alta/de/categorias&result=ok');
		else 
			header('location: ../proceso.php?op=alta/de/categorias&result=bad');
	}
	//Editar registro
	else if($_GET["action"] == "update"){
		//Get ID
		$oRegistro = $oDB->categoria[secureParamToSql($_POST[$sId])]; 

		if($oRegistro){
			$aData = [
				'nombre' => $_POST['nombre'],
				'slug' => to_prety_url($_POST['nombre']),
				'parent_id' => (isset($_POST['parent_id'])?$_POST['parent_id']:0),
				'tipo' => $_POST['tipo'],
				'publicada' => $_POST['publicada'],
				'edit_user' => $_POST['idOwner'],
				'edit_date' => date("Y-m-d H:m:s"),
			];

			//+ Extra fields
			if($_FILES['imagen']['name']  != ""){
				$foo = new upload($_FILES['imagen']);

				if ($foo->uploaded){

					$foo->file_name_body_add = '-large';
					$foo->image_resize       = true;
					$foo->image_x 			 = _thumb_lg_sizex;
	   				if(_thumb_lg_sizey===0){
	   					$foo->image_ratio_y    = true;
					} else{
						$foo->image_ratio_crop = true;
	   					$foo->image_y          = _thumb_lg_sizey;
					}
					$foo->Process(_global_catpath);
					if(!$foo->processed) { echo 'error : ' . $foo->error; }
					$aData['imagen']		 = $foo->file_dst_name_body.'.'.$foo->file_dst_name_ext;

					$foo->file_name_body_add = '-medium';
					$foo->image_resize       = true;
					$foo->image_x            = _thumb_md_sizex;
					if(_thumb_md_sizey===0){
						$foo->image_ratio_y	   = true;
					} else {
						$foo->image_ratio_crop = true;
	   					$foo->image_y          = _thumb_lg_sizey;
					}
					$foo->Process(_global_catpath);
					$aData['imagenmd']		 = $foo->file_dst_name_body.'.'.$foo->file_dst_name_ext;

					$foo->file_name_body_add = '-small';
					$foo->image_resize       = true;
					$foo->image_x            = _thumb_sm_sizex;
					if(_thumb_sm_sizey===0){
						$foo->image_ratio_y	   = true;
					} else {
						$foo->image_ratio_crop = true;
						$foo->image_y          = _thumb_sm_sizey;
					}
					$foo->Process(_global_catpath);
					$aData['imagensm']		 = $foo->file_dst_name_body.'.'.$foo->file_dst_name_ext;

					$foo->Clean();
				}
			}

			if(isset($_POST['alt'])&&$_POST['alt']!='')
				$adata['alt'] = $_POST['alt'];

			//Update record in database
			$upd = $oRegistro->update($aData);

		}

		if($upd)
			header('location: ../proceso.php?op=panel/categorias&result=ok');
		else
			header('location: ../proceso.php?op=panel/categorias&result=bad');
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{

		//Defino el array donde voy a mandar los resultados de vuelta
		$aResult = [];

		//Get ID
		$oRegistro = $oDB->categoria[secureParamToSql($_POST['id'])];
		if($oRegistro){
			$aData = [
				'kill_user' => $_POST['idOwner'],		
				'kill_date' => date("Y-m-d H:m:s"),
				'eliminado' => 1
			];
			//"Delete" from database
			$aResult['success'] = $oRegistro->update($aData);
		} else {
			$aResult['success'] = false;
		}

		//Return data para comprobar si se elimino ya que se hace con AJAX necesitamos json_encode
		print(json_encode($aResult));
		
	}

}
catch(Exception $ex)
{
    //Return error message
	$aResult = [];
	$aResult['Result'] = "ERROR";
	$aResult['Message'] = $ex->getMessage();
	print_r(json_encode($aResult));
}
	
?>