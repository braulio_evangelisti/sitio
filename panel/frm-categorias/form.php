<?php

  $urlubic = "";
  require('func.includes/seguridad.php');
  include_once("func.includes/config.inc.php");

  $op = secureParamToSql($_GET['op']);
  
  if($op == 'alta/de/categorias') {
    $action   = 'create';
    $title    = 'Nueva categoria';
  } else if($op == 'edicion/de/categorias') {
    $action    = 'update';
    $title     = 'Editar categoria';

    $aCategoria = $oDB->categoria[secureParamToSql($_GET['id'])]; 
  }

  if(isset($_GET['result']))
    switch ($_GET['result']) {
      case 'bad':
          $result = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button><i class='fas fa-exclamation-triangle'></i> Ocurrió un error durante la operación.</div>";
      break;
      case 'ok':
          $result = "<div class='alert alert-success alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button><i class='fas fa-check-circle'></i> La operación se ha realizado con éxito.</div>";
      break;
    }
  
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?=_global_metaDesc?>">
    <meta name="keywords" content="<?=_global_metaKeys?>">
    <meta name="author" content="<?=_global_metaAuth?>">
    <link rel="icon" href="../img/favicon.ico">

    <title><?=$title?></title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    
    <!-- Autocomplete -->
    <link rel="stylesheet" href="js/EasyAutocomplete-1.3.5/easy-autocomplete.min.css"> 
    
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
    
  </head>

  <body>

    <?php include('menu.php'); ?>

    <div class="container-fluid">

      <div class="row">

        <?php include('left_nav.php'); ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="proceso.php?op=panel/administracion">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Categorias</li>
            </ol>
          </nav>

          <h2>
            <?=$title?> 
            <a href="proceso.php?op=panel/categorias" class="btn btn-secondary btn-sm float-right">Volver</a>
          </h2>

          <div>
            <?=isset($result)?$result:''?>
            <form action="frm-categorias/procesar.php?action=<?=$action?>" class="form row" method="POST" enctype="multipart/form-data" role="form">
              
              <input type="hidden" name="idOwner" class="idOwner" value="<?=$_SESSION['id']; ?>" />
              <?php if(isset($aCategoria)){ ?>
                <input type="hidden" name="id" id="id" value="<?=$aCategoria['id']; ?>" />
                <input type="hidden" name="imagenOld" id="imagenOld" value="<?=$aCategoria['imagen']; ?>" />
              <?php } ?>

              <div class="col-8">

                <div class="form-group required">
                  <label for="nombre" class="control-label">Nombre</label>
                  <input type="text" name="nombre" class="form-control" id="nombre" value="<?=isset($_POST['nombre'])?$_POST['nombre']:isset($aCategoria['nombre'])?$aCategoria['nombre']:false?>" placeholder="Ingrese el nombre de la categoria" required autofocus>
                  <!-- <small class="help-block">Campo requerido</small> -->
                </div>            
              
                <div class="form-group required">
                  <label for="tipo" class="control-label">Tipo</label>
                  <select class="form-control input-sm" id="tipo" name="tipo" required>
                    <option value="">Seleccione</option>
                    <option value="producto" <?=(isset($_POST['tipo'])&&$_POST['tipo']=='producto')||(isset($aCategoria['tipo'])&&$aCategoria['tipo']=='producto')?'selected="selected"':false?> >Producto</option>
                    <option value="novedad" <?=(isset($_POST['tipo'])&&$_POST['tipo']=='novedad')||(isset($aCategoria['tipo'])&&$aCategoria['tipo']=='novedad')?'selected="selected"':false?> >Novedad</option>
                    <option value="institucional" <?=(isset($_POST['tipo'])&&$_POST['tipo']=='institucional')||(isset($aCategoria['tipo'])&&$aCategoria['tipo']=='institucional')?'selected="selected"':false?> >Institucional</option>
                    <option value="portfolio" <?=(isset($_POST['tipo'])&&$_POST['tipo']=='portfolio')||(isset($aCategoria['tipo'])&&$aCategoria['tipo']=='portfolio')?'selected="selected"':false?> >Portfolio</option>
                    <option value="servicios" <?=(isset($_POST['tipo'])&&$_POST['tipo']=='servicios')||(isset($aCategoria['tipo'])&&$aCategoria['tipo']=='servicios')?'selected="selected"':false?> >Servicios</option>
                  </select>
                  <small class="help-block">Campo requerido</small>
                </div>

                <div class="form-group">
                  <label for="tipo" class="control-label">Catergoria padre</label>
                  <select class="form-control input-sm" id="parent_id" name="parent_id">
                    <option value="" selected disabled hidden>Seleccione...</option>
                    <?php 
                      $catPadres = $oDB->categoria()->select('id, parent_id, nombre, tipo')->where("id > ?", 1)->and("parent_id", null)->and("publicada = ?", "SI")->and("eliminado = ?", 0); 
                      if($catPadres){
                        foreach($catPadres as $catPadre){
                      ?>
                        <option value="<?=$catPadre['id']?>" <?=(isset($_POST['parent_id'])&&$_POST['parent_id']==$catPadre['id'])||(isset($aCategoria['parent_id'])&&$aCategoria['parent_id']==$catPadre['id'])?'selected':false?> ><?=ucfirst($catPadre['nombre']) ." - (". $catPadre['tipo'] .")"?></option>
                      <?php 
                        }  
                      } 
                    ?>
                  </select>
                  <small class="help-block">Para crear una subcategoria seleccione una categoria padre. De lo contrario se creará una categoria principal.</small>
                </div>

              </div> <!-- Fin col-8 -->

              <sidebar class="col-4">

                <div class="card border-primary mb-3">
                  <div class="card-header">
                    <i class="fas fa-save"></i> Publicar
                  </div>
                  <div class="card-body">
                    <div class="form-group">
                      <label for="publicada"><i class="fas fa-eye"></i> Estado</label><br>
                      <input type="radio" name="publicada" value="SI" <?=(isset($_POST['publicada'])&&$_POST['publicada']=='SI')||(isset($aCategoria['publicada'])&&$aCategoria['publicada']=='SI')||$action=='create'?'checked':false?> > Publicada
                      <input type="radio" name="publicada" value="NO" <?=(isset($_POST['publicada'])&&$_POST['publicada']=='NO')||(isset($aCategoria['publicada'])&&$aCategoria['publicada']=='NO')?'checked':false?> > Borrador
                    </div>
                    <?php if($action == 'update'){ ?>
                    <p>
                      <i class="fas fa-calendar-alt"></i> Creada el: <b><?=fecha_completa($aCategoria['add_date']); ?></b>
                    </p>
                    <?php } ?>
                  </div>
                  <div class="card-footer text-right"">
                    <?php if($action == 'update'){ ?>
                      <a href="#" class="delete-row float-left btn-link" data-id="<?=isset($aCategoria['id'])?$aCategoria['id']:false?>" alt="<?=$aCategoria['nombre']; ?>" title="Eliminar" style="margin-top:6px;"><i class="fas fa-trash"></i> Eliminar</a>
                    <?php } ?>
                    <input name="procesar" type="submit" id="procesar" value="Guardar" class="btn btn-primary btn-sm" />                  
                  </div>
                </div>

                <div class="card border-primary">
                  <div class="card-header">
                    <i class="fas fa-image"></i> &nbsp;Imagen
                  </div>
                  <div class="card-body">
                    <div class="form-group">
                      <label for="imagen" class="custom-file"> 
                        <input type="file" name="imagen" id="imagen" value="<?=isset($aCategoria['imagen'])?$aCategoria['imagen']:false?>" class="custom-file-input" />
                        <span class="custom-file-control">Seleccionar imagen...</span>
                      </label>
                      <small class="help-block">Campo requerido. <br>590x415px es el tamaño sugerido para una mejor visualización en el sitio.</small>
                    </div>
                    <div class="form-group">
                      <input type="text" name="alt" class="form-control" id="alt" value="<?=isset($aCategoria['alt'])?$aCategoria['alt']:false?>" placeholder="Texto alternativo de la imagen">
                    </div>
                  </div>
                  <?php if(isset($aCategoria['imagensm'])){ ?>
                    <img src="<?=_global_caturl.$aCategoria['imagensm']?>" class="card-img-bottom">
                  <?php } ?>
                </div>

              </sidebar> <!-- Fin sidebar -->

            </form>

          </div>

        </main>

      </div>

    </div> <!-- /container -->

    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true" id="delete-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Se necesita confirmación</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-sm" id="modal-btn-si">Si</button>
            <button type="button" class="btn btn-secondary btn-sm" id="modal-btn-no" data-dismiss="modal">No</button>
          </div>
        </div>
      </div>
    </div>
    
    <?php include('footer.php'); ?>

    <!-- Otras funciones -->

    <script type="text/javascript">
      $(document).ready(function() {
        //Boton eliminar
        $('.card-footer').on( 'click', 'a.delete-row', function(event) {

          var id      = $(this).attr('data-id');
          var idOwner = $('#idOwner').text();
          var nombre  = $(this).attr('alt');
          var url     = 'frm-categorias/procesar.php?action=delete';
          var options = {
            backdrop: 'static'
          };

          $('#delete-modal .modal-body').html("Esta seguro que desea eliminar a <b>" + nombre + "</b>?");
          $('#delete-modal').modal(options);
          $("#modal-btn-si").on("click", function(){

            $.ajax({
              url: url,
              type: 'POST',
              dataType: 'json',
              data: { id: id, idOwner: idOwner },
              success: function(data){
                //alert(JSON.stringify(data, null, 4));
                if(data.success){
                  $(location).attr('href', 'proceso.php?op=panel/categorias');
                }
              },
              error: function(data){
                alert('Ocurrio un error eliminando el registro');
              }

            });

          });

          event.preventDefault();

        });

      });
    </script>

  </body>
</html>
