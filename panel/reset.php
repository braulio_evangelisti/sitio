<?php 
  
$urlubic = "";

include_once($urlubic."func.includes/config.inc.php");

switch ($_GET['estado']) {
  case 1:
      $estado = "<div class='alert alert-danger'><strong>Error!</strong> Todos los campos son requeridos.</div>";
      break;
  case 2:
      $estado = "<div class='alert alert-danger'><strong>Error!</strong> No estas autorizado a resetear el password de esta cuenta.</div>";
      break;
  case 3:
      $estado = "<div class='alert alert-warning'><strong>Error!</strong> Los campos de contraseña deben ser iguales.</div>";
      break;
  case 4:
      $estado = "<div class='alert alert-success'><strong>La sesión ha finalizado!</strong><br>Ingrese nuevamente desde aqui.</div>";
      break;
  case 5:
      $estado = "<div class='alert alert-warning'><strong>Error!</strong> Verifique el campo captcha.</div>";
      break;
  case 6:
      $estado = "<div class='alert alert-success'><strong>Proceso exitoso!</strong><br>Su password ha sido reseteado correctamente. Por favor ingrese con su nuevo password.</div>";
      break;
  case 7:
      $estado = "<div class='alert alert-danger'><strong>Error!</strong> Algo salió mal al resetear el password.</div>";
      break;
}

if(isset($_POST['procesar'])){
  
  $reCaptcha = new \ReCaptcha\ReCaptcha(_privatekey);
  $oResponse = $reCaptcha->setExpectedHostname($_SERVER['SERVER_NAME'])
              ->verify($_POST["g-recaptcha-response"], $_SERVER["REMOTE_ADDR"]);

  if (empty($_POST["g-recaptcha-response"])){
    /* Si el captcha está vacío */
    header("Location: index.php?op=reset&".$_POST['encrypt']."&estado=5");
    exit();
  } else if(!$oResponse->isSuccess()){
    /* Si el captcha es incorrecto */ 
    header("Location: index.php?op=reset&".$_POST['encrypt']."&estado=5");
    exit();
  } else {
    /* Si el captcha es correcto */

    $encrypt = '';
    if(!empty($_POST['password']) && !empty($_POST['confirm_password']) && !empty($_POST['encrypt'])){
      $password = secureParamToSql($_POST['password']);
      $confirm_password = secureParamToSql($_POST['confirm_password']);
      $encrypt = secureParamToSql($_POST['encrypt']);
      //password and confirm password comparison
      if($password !== $confirm_password){
        // SEGUIR ACA
        header("Location: proceso.php?op=reset&encrypt=".$encrypt."&estado=3");
        exit();
      }else{
        //check whether identity code exists in the database
        // sha1(90*13+$row['idUsuario']); seguir aca
        $oUser = $oDB->usuario()->select("id")->where("SHA1(90*13+id) = ?, eliminado = ?", $encrypt, 0)->fetch();
        //$oUser = $objGeneral->listar_registros("select id from usuario where SHA1(90*13+id) = '".$encrypt."' AND eliminado = 0")[0];
        if(!empty($oUser)){
          //update data with new password
          $aData = [ 'password' => sha1($password) ];
          // $update = $objGeneral->actualizar_registro($aData, "usuarios", "idUsuario", $oUser['idUsuario']);
          $update = $oUser->update($aData);
          if($update){
              header("Location: index.php?estado=8");
              exit();
          }else{
            header("Location: proceso.php?op=reset&encrypt=".$encrypt."&estado=7");
            exit();
          }
        }else{
          header("Location: proceso.php?op=reset&encrypt=".$encrypt."&estado=2");
          exit();
        }
      }
    }else{
      header("Location: proceso.php?op=reset&encrypt=".$encrypt."&estado=1");
      exit();
    }

  } /*Captcha*/

} /*Procesar*/

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Resetear password</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="<?=_global_metaDesc?>">
    <meta name="author" content="<?=_global_metaAuth?>">
    <meta name="keywords" content="<?=_global_metaKeys?>">
    <link rel="icon" href="../img/favicon.ico">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">

    <script type="text/javascript">
      /* 
      Para cambiar el theme del captcha.
      themes: red|white|blackglass|clean
      */
      var RecaptchaOptions = {
        theme : 'clean',
        lang  : 'es',
      };
    </script>

  </head>

  <body>

    <div class="container">

     <form id="signin" class="form-signin" role="form" method="POST" action="proceso.php?op=reset">
        <h2 class="form-signin-heading"><i class="glyphicon glyphicon-lock"></i> Resetear</h2>

          <?=isset($estado)?$estado:false;?>

          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password" required>
          </div>  
          
          <div class="form-group has-feedback">
            <input type="password" name="confirm_password" class="form-control" placeholder="Repetir Password" required>
          </div>  

          <!-- Mostrar el campo captcha -->
          <!-- <?php echo recaptcha_get_html(_publickey); ?> -->      
          <div class="g-recaptcha" data-sitekey="<?=_publickey?>"></div>
          <input type="hidden" name="encrypt" value="<?=secureParamToSql($_GET['encrypt']);?>"/>
          <br>
          <a href="index.php" >Volver al login</a>  
          
          <br>
          &nbsp;
          <br>

          <button class="btn btn-lg btn-primary btn-block" type="submit" name="procesar">Enviar</button>

      </form>

    </div> <!-- /container -->
    <footer class="footer">
      <p>© <?php echo date('Y'); ?></p>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
    <script>$(document).on('ready', function() {});</script>

  </body>
</html>