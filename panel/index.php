<?php 
  
$urlubic = "";

include_once($urlubic."func.includes/config.inc.php");

if(isset($_GET['estado']))
  switch ($_GET['estado']) {
    case 1:
        $estado = "<div class='alert alert-danger'><strong>Error!</strong> Email incorrecto.</div>";
        break;
    case 2:
        $estado = "<div class='alert alert-danger'><strong>Error!</strong> Contraseña incorrecta.</div>";
        break;
    case 3:
        $estado = "<div class='alert alert-warning'><strong>Error!</strong> Ud. no tiene permiso para ver esta página.</div>";
        break;
    case 4:
        $estado = "<div class='alert alert-success'><strong>La sesión ha finalizado!</strong><br>Ingrese nuevamente desde aqui.</div>";
        break;
    case 5:
        $estado = "<div class='alert alert-warning'><strong>Error!</strong> Verifique el campo captcha. </div>";
        break;
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?=_global_metaDesc?>">
    <meta name="author" content="<?=_global_metaAuth?>">
    <meta name="keywords" content="<?=_global_metaKeys?>">
    <link rel="icon" href="../img/favicon.ico">

    <title>Iniciar sesión</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">

  </head>

  <body>

    <div class="container">

     <div class="row">
       <form id="signin" class="form-signin" role="form" method="POST" action="proceso.php?op=login">
        <h2 class="form-signin-heading"><i class="glyphicon glyphicon-lock"></i> Iniciar sesión</h2>

          <?=isset($estado)?$estado:false;?>

          <input type="email" name="email" class="form-control" placeholder="Email" required autofocus>
          
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password" required>
          </div>  

          <!-- Mostrar el campo captcha -->
          <div class="g-recaptcha" data-sitekey="<?=_publickey?>"></div>
          <br>

          <input type="checkbox" name="recordar" id="recordar" value="true" /> No cerrar sesión  
          &middot;
          <a href="#forget" onclick="forgetpassword();" >Olvide mi contraseña</a>  
          
          <br>
          &nbsp;
          <br>

          <button class="btn btn-lg btn-primary btn-block" type="submit" name="procesar">Ingresar</button>

      </form>

      <br>

      <form id="passwd" class="form-signin" role="form" method="POST" action="proceso.php?op=forgot" style="display:none;">
        <input id="email" class="form-control" name="email" type="text"  placeholder="Email">
        <br>
        <div class="g-recaptcha" data-sitekey="<?=_publickey?>"></div>
        <br>
        <a href="#login" onclick="login();" >Volver al login</a>   
        <br>
        &nbsp;
        <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="procesar">Reset Password</button>
      </form>
     </div>

    </div> <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
    <script>
      $(document).on('ready', function() {});
      function forgetpassword() {
        $("#signin").hide();
        $("#passwd").show();
      }
      function login() {
        $("#passwd").hide();
        $("#signin").show();
      }
      function checkHash(){
        if(window.location.hash) {
          var hash = window.location.hash.substring(1);
          if(hash == 'login'){
            login();
          } else if (hash == 'forget'){
            forgetpassword();
          }
        }
      }
      checkHash();
    </script>
  </body>
</html>