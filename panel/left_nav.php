<nav class="col-md-2 d-none d-md-block bg-light sidebar">
  <div class="sidebar-sticky">
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "administracion")!==false?'active':''?>" href="proceso.php?op=panel/administracion">
          <i class="fas fa-chart-bar"></i>
          Escritorio <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "slide")!==false?'active':''?>" href="proceso.php?op=panel/slider">
          <i class="fas fa-image"></i>
          Slider
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "categoria")!==false?'active':''?>" href="proceso.php?op=panel/categorias">
          <i class="fas fa-tags"></i>
          Categorias
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "novedades")!==false?'active':''?>" href="proceso.php?op=panel/novedades">
          <i class="far fa-newspaper"></i>
          Novedades
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "empresa")!==false?'active':''?>" href="proceso.php?op=panel/empresa">
          <i class="fas fa-industry"></i>
          Institucional
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "servicio")!==false?'active':''?>" href="proceso.php?op=panel/servicios">
          <i class="fas fa-tasks"></i>
          Servicios
        </a>
      </li>
    </ul>

    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
      <span>Empresa</span>
      <a class="d-flex align-items-center text-muted" href="#">
        <i class="fas fa-shopping-basket"></i>
      </a>
    </h6>
    <ul class="nav flex-column mb-2"> 
        <li class="nav-item">
	        <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "producto")!==false?'active':''?>" href="proceso.php?op=panel/productos">
	            <i class="fas fa-shopping-basket"></i>
	            Productos
	        </a>
        </li>
        <li class="nav-item">
	        <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "sucursales")!==false?'active':''?>" href="proceso.php?op=panel/sucursales">
	            <i class="fas fa-home"></i>
	            Sucursales
	        </a>
        </li>
    </ul>

    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
      <span>Usuarios</span>
      <a class="d-flex align-items-center text-muted" href="#">
        <i class="fas fa-user"></i>
      </a>
    </h6>
    <ul class="nav flex-column mb-2">
      <?php if($_SESSION['NIVEL'] == 0){ ?>
        <li class="nav-item">
          <a class="nav-link" href="proceso.php?op=panel/usuarios">
            <i class="fas fa-users"></i>
            Usuarios
          </a>
        </li>
      <?php } ?>
      <li class="nav-item">
        <a class="nav-link" href="proceso.php?op=panel/clientes">
          <i class="fas fa-briefcase"></i>
          Clientes
        </a>
      </li>
    </ul>
  </div>
</nav>
<!-- <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
  
  <ul class="nav nav-pills flex-column">
    <li class="nav-item">
      <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "administracion")!==false?'active':''?>" href="proceso.php?op=panel/administracion">
        <i class="fas fa-chart-bar"></i>
        Overview <span class="sr-only">(current)</span>
      </a>
    </li>
  </ul>

  <ul class="nav nav-pills flex-column">
    <li class="nav-item">
      <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "slide")!==false?'active':''?>" href="proceso.php?op=panel/slider">
        <i class="fas fa-image"></i>
        Slider
      </a>
    </li>
  </ul>

  <ul class="nav nav-pills flex-column">
    <li class="nav-item">
      <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "categoria")!==false?'active':''?>" href="proceso.php?op=panel/categorias">
        <i class="fas fa-tags"></i>
        Categorias
      </a>
    </li>
  </ul>

  <ul class="nav nav-pills flex-column">
    <li class="nav-item">
      <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "producto")!==false?'active':''?>" href="proceso.php?op=panel/productos">
        <i class="fas fa-shopping-basket"></i>
        Productos
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "noticia")!==false?'active':''?>" href="proceso.php?op=panel/novedades">
        <i class="far fa-newspaper"></i>
        Novedades
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "empresa")!==false?'active':''?>" href="proceso.php?op=panel/empresa">
        <i class="fas fa-industry"></i>
        Empresa
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "servicio")!==false?'active':''?>" href="proceso.php?op=panel/servicios">
        <i class="fas fa-tasks"></i>
        Servicios
      </a>
    </li>
  </ul>
</nav> -->