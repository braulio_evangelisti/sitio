<?php

	switch($_REQUEST['op'])
	{
	// Página de ingreso al sistema de administración ----------------
		case "login":
		$url = "func.includes/validar.php";
		break;
		case "forgot":
		$url = "func.includes/forgot.php";
		break;
		case "reset":
		$url = "func.includes/reset.php";
		break;
		case "register":
		$url = "func.includes/register.php";
		break;
		
		case "panel/administracion":
		$url = "adminPanel.php";
		break;
		
		case "help":
		$url = "help.php";
		break;

	// Desloguearse del sistema de administración --------------------
		case "logout":
		$url = "func.includes/salir.php";
		break;

	// ==============================================================================

	// ==============================================================================
	
	// ABM Usuarios ------------------------------------------------------------------
		case "panel/usuarios": $url = "frm-usuarios/listar.php"; break;
		case "alta/de/usuarios": $url = "frm-usuarios/form.php"; break;
		case "edicion/de/usuarios": $url = "frm-usuarios/form.php"; break;
		case "eliminacion/de/usuarios": $url = "frm-usuarios/procesar.php"; break;

	// ABM Categorias ------------------------------------------------------------------
		case "panel/categorias": $url = "frm-categorias/listar.php"; break;
		case "alta/de/categorias": $url = "frm-categorias/form.php"; break;
		case "edicion/de/categorias": $url = "frm-categorias/form.php"; break;
		case "eliminacion/de/categorias": $url = "frm-categorias/procesar.php"; break;

	// ABM Institucional ------------------------------------------------------------------
		case "panel/empresa": $url = "frm-empresa/listar.php"; break;
		case "alta/de/empresa": $url = "frm-empresa/form.php"; break;
		case "edicion/de/empresa": $url = "frm-empresa/form.php"; break;
		case "eliminacion/de/empresa": $url = "frm-empresa/procesar.php"; break;

	// ABM Servicios ------------------------------------------------------------------
		case "panel/servicios": $url = "frm-servicios/listar.php"; break;
		case "alta/de/servicios": $url = "frm-servicios/form.php"; break;
		case "edicion/de/servicios": $url = "frm-servicios/form.php"; break;
		case "eliminacion/de/servicios": $url = "frm-servicios/procesar.php"; break;

	// ABM Novedades ------------------------------------------------------------------
		case "panel/novedades": $url = "frm-novedades/listar.php"; break;
		case "alta/de/novedades": $url = "frm-novedades/form.php"; break;
		case "edicion/de/novedades": $url = "frm-novedades/form.php"; break;
		case "eliminacion/de/novedades": $url = "frm-novedades/procesar.php"; break;

	// ABM Galerias
		case "panel/galerias": $url = "frm-galerias/listar.php"; break;
		case "alta/de/galerias": $url = "frm-galerias/form.php"; break;
		case "edicion/de/galerias": $url = "frm-galerias/form.php"; break;
		case "eliminacion/de/galerias": $url = "frm-galerias/procesar.php"; break;

	// ABM Slider ------------------------------------------------------------------
		case "panel/slider": $url = "frm-slider/listar.php"; break;
		case "alta/de/slider": $url = "frm-slider/form.php"; break;
		case "edicion/de/slider": $url = "frm-slider/form.php"; break;
		case "eliminacion/de/slider": $url = "frm-slider/procesar.php"; break;

	// ABM Portfolio ------------------------------------------------------------------
		case "panel/portfolio": $url = "frm-portfolio/listar.php"; break;
		case "alta/de/portfolio": $url = "frm-portfolio/form.php"; break;
		case "edicion/de/portfolio": $url = "frm-portfolio/form.php"; break;
		case "eliminacion/de/portfolio": $url = "frm-portfolio/procesar.php"; break;

	// ABM Productos ------------------------------------------------------------------
		case "panel/productos": $url = "frm-productos/listar.php"; break;
		case "alta/de/productos": $url = "frm-productos/form.php"; break;
		case "edicion/de/productos": $url = "frm-productos/form.php"; break;
		case "eliminacion/de/productos": $url = "frm-productos/procesar.php"; break;

	// ABM Clientes ------------------------------------------------------------------
		case "panel/clientes": $url = "frm-clientes/listar.php"; break;
		case "alta/de/clientes": $url = "frm-clientes/form.php"; break;
		case "edicion/de/clientes": $url = "frm-clientes/form.php"; break;
		case "eliminacion/de/clientes": $url = "frm-clientes/procesar.php"; break;

	// ABM Sucursales ------------------------------------------------------------------
		case "panel/sucursales": $url = "frm-sucursales/listar.php"; break;
		case "alta/de/sucursales": $url = "frm-sucursales/form.php"; break;
		case "edicion/de/sucursales": $url = "frm-sucursales/form.php"; break;
		case "eliminacion/de/sucursales": $url = "frm-sucursales/procesar.php"; break;

	//Valor por defecto
		
		default:
		$url = "404.php";
	}

	require("$url");

?>