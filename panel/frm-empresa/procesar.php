<?php

try {

	$urlubic = "../";
	include_once($urlubic."func.includes/config.inc.php");

	/* 
	* Definir tabla y clave
	*/
	$sTable = "empresa";
	$sId 	= "id";

	//Nuevo registro
	if($_GET["action"] == "create"){
		$aData = [
			'slug' => to_prety_url($_POST['titulo']),
			'titulo' => $_POST['titulo'],
			'bajada' => addslashes($_POST['bajada']),
			'descripcion' => addslashes($_POST['descripcion']),
			'categoria_id' => (isset($_POST['categoria_id'])?$_POST['categoria_id']:null),
			'publicada' => $_POST['publicada'],
			'add_user' => $_POST['idOwner'],
			'add_date' => date("Y-m-d H:m:s"),
			'eliminado' => 0
		];
		//+ Extra fields
		if($_FILES['imagen']['name']  != ""){
			$foo = new upload($_FILES['imagen']);

			if ($foo->uploaded){

				$foo->file_name_body_add   = '-large';
				$foo->image_resize         = true;
				$foo->image_x 			   = _thumb_lg_sizex;
				if(_thumb_lg_sizey===0){
   					$foo->image_ratio_y    = true;
				} else{
					$foo->image_ratio_crop = true;
   					$foo->image_y          = _thumb_lg_sizey;
				}
				$foo->Process(_global_instpath);
				$aData['imagen']		   = $foo->file_dst_name_body.'.'.$foo->file_dst_name_ext;

				$foo->file_name_body_add   = '-medium';
				$foo->image_resize         = true;
				$foo->image_x              = _thumb_md_sizex;
				if(_thumb_md_sizey===0){
					$foo->image_ratio_y	   = true;
				} else {
					$foo->image_ratio_crop = true;
   					$foo->image_y          = _thumb_lg_sizey;
				}
				$foo->Process(_global_instpath);
				$aData['imagenmd']		   = $foo->file_dst_name_body.'.'.$foo->file_dst_name_ext;

				$foo->file_name_body_add   = '-small';
				$foo->image_resize         = true;
				$foo->image_x          	   = _thumb_sm_sizex;
				if(_thumb_sm_sizey===0){
					$foo->image_ratio_y	   = true;
				} else {
					$foo->image_ratio_crop = true;
					$foo->image_y          = _thumb_sm_sizey;
				}
				$foo->Process(_global_instpath);
				$aData['imagensm']		   = $foo->file_dst_name_body.'.'.$foo->file_dst_name_ext;

				$foo->Clean();
			}
		} else {
			unset($_POST['imagen']);
		}

		if(isset($_POST['alt'])&&$_POST['alt']!='')
			$adata['alt'] = $_POST['alt'];

		if($_FILES['archivo']['name']!=""){
			$archivo = time()."_".$_FILES['archivo']['name'];
			$destino =  _global_filepath.$archivo ;
			move_uploaded_file($_FILES['archivo']['tmp_name'],$destino);
			$aData['archivo'] = $archivo;
		}

		//Insert record into database
		$ins = $oDB->empresa()->insert($aData);
		
		if(isset($ins['id']))
			header('location: ../proceso.php?op=alta/de/empresa&result=ok');
		else
			header('location: ../proceso.php?op=alta/de/empresa&result=bad');
	}
	//Editar registro
	else if($_GET["action"] == "update"){
		//Get ID
		$oRegistro = $oDB->empresa[secureParamToSql($_POST[$sId])]; 
		
		if($oRegistro){
			$aData = [
				'slug' => to_prety_url($_POST['titulo']),
				'titulo' => $_POST['titulo'],
				'bajada' => addslashes($_POST['bajada']),
				'descripcion' => addslashes($_POST['descripcion']),
				'categoria_id' => (isset($_POST['categoria_id'])?$_POST['categoria_id']:null),
				'publicada' => $_POST['publicada'],
				'edit_user' => $_POST['idOwner'],
				'edit_date' => date("Y-m-d H:m:s"),
			];

			// Extra fields
			if($_FILES['imagen']['name']  != ""){
				$foo = new upload($_FILES['imagen']);

				if ($foo->uploaded){

					$foo->file_name_body_add = '-large';
					$foo->image_resize       = true;
					$foo->image_x 			 = _thumb_lg_sizex;
	   				if(_thumb_lg_sizey===0){
	   					$foo->image_ratio_y    = true;
					} else{
						$foo->image_ratio_crop = true;
	   					$foo->image_y          = _thumb_lg_sizey;
					}
					$foo->Process(_global_instpath);
					if(!$foo->processed) { echo 'error : ' . $foo->error; }
					$aData['imagen']		 = $foo->file_dst_name_body.'.'.$foo->file_dst_name_ext;

					$foo->file_name_body_add = '-medium';
					$foo->image_resize       = true;
					$foo->image_x            = _thumb_md_sizex;
					if(_thumb_md_sizey===0){
						$foo->image_ratio_y	   = true;
					} else {
						$foo->image_ratio_crop = true;
	   					$foo->image_y          = _thumb_lg_sizey;
					}
					$foo->Process(_global_instpath);
					$aData['imagenmd']		 = $foo->file_dst_name_body.'.'.$foo->file_dst_name_ext;

					$foo->file_name_body_add = '-small';
					$foo->image_resize       = true;
					$foo->image_x            = _thumb_sm_sizex;
					if(_thumb_sm_sizey===0){
						$foo->image_ratio_y	   = true;
					} else {
						$foo->image_ratio_crop = true;
						$foo->image_y          = _thumb_sm_sizey;
					}
					$foo->Process(_global_instpath);
					$aData['imagensm']		 = $foo->file_dst_name_body.'.'.$foo->file_dst_name_ext;

					$foo->Clean();
				}
			} else {
				unset($_POST['imagen']);
			}

			if(isset($_POST['alt'])&&$_POST['alt']!='')
				$aData['alt'] = $_POST['alt'];

			if($_FILES['archivo']['name']!=""){
				$archivo = time()."_".$_FILES['archivo']['name'];
				$destino =  _global_filepath.$archivo ;
				move_uploaded_file($_FILES['archivo']['tmp_name'],$destino);
				$aData['archivo'] = $archivo;
			}

			//Update record in database
			$upd = $oRegistro->update($aData);
		}
		
		if($upd)
			header('location: ../proceso.php?op=panel/empresa&result=ok');
		else
			header('location: ../proceso.php?op=panel/empresa&result=bad');
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete"){
		//Defino el array donde voy a mandar los resultados de vuelta
		$aResult = [];

		//Get ID
		$oRegistro = $oDB->empresa[secureParamToSql($_POST['id'])];
		if($oRegistro){
			$aData = [
				'kill_user' => $_POST['idOwner'],		
				'kill_date' => date("Y-m-d H:m:s"),
				'eliminado' => 1
			];
			//"Delete" from database
			$aResult['success'] = $oRegistro->update($aData);
		} else {
			$aResult['success'] = false;
		}

		//Return data para comprobar si se elimino ya que se hace con AJAX necesitamos json_encode
		print(json_encode($aResult));
		
	}

}
catch(Exception $ex)
{
    //Return error message
	$result = [];
	$result['Result'] = "ERROR";
	$result['Message'] = $ex->getMessage();
	print_r(json_encode($result));
}
	
?>