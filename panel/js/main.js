//Inicializa autocompletar 
var options = {
	adjustWidth: false,
	url: function(phrase) {
		return "api/buscar.php?q=" + phrase + "&format=json";
	},
	getValue: "titulo",
	template: {
		type: "description",
		fields: {
			description: "descripcion"
		}
	},
	list: {
		maxNumberOfElements: 10,
		onChooseEvent: function() {
			var url = $("#buscar").getSelectedItemData().url;
			window.location.href = url;
		}	
	}
};

$("#buscar").easyAutocomplete(options);

// Reutilizar modar confirm