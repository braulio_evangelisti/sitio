<?php

  $urlubic = "";
  require('func.includes/seguridad.php');
  include_once("func.includes/config.inc.php");

  $op = secureParamToSql($_GET['op']);
  
  if($op == 'alta/de/productos') {
    $action   = 'create';
    $title    = 'Nuevo producto';
  } else if($op == 'edicion/de/productos') {
    $action    = 'update';
    $title     = 'Editar producto';
    $aProducto = $oDB->producto[secureParamToSql($_GET['id'])]; 
  }

  include_once("result_message.php");
  
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?=_global_metaDesc?>">
    <meta name="keywords" content="<?=_global_metaKeys?>">
    <meta name="author" content="<?=_global_metaAuth?>">
    <link rel="icon" href="../img/favicon.ico">

    <title><?=$title?></title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">
    
    <!-- Autocomplete -->
    <link rel="stylesheet" href="js/EasyAutocomplete-1.3.5/easy-autocomplete.min.css"> 
    
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
    
  </head>

  <body>

    <?php include('menu.php'); ?>

    <div class="container-fluid">

      <div class="row">

        <?php include('left_nav.php'); ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="proceso.php?op=panel/administracion">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Productos</li>
            </ol>
          </nav>

          <h2>
            <?=$title?> 
            <a href="proceso.php?op=panel/productos" class="btn btn-secondary btn-sm float-right">Volver</a>
          </h2>

          <div>
            <?=isset($result)?$result:''?>
            <form action="frm-productos/procesar.php?action=<?=$action?>" class="form row" method="POST" enctype="multipart/form-data" role="form">
          
              <input type="hidden" name="idOwner" class="idOwner" value="<?=$_SESSION['id']?>" />
              <?php if(isset($aProducto)){ ?>
                <input type="hidden" name="id" id="id" value="<?=$aProducto['id']?>" />
              <?php } ?>

              <div class="col-8">

              <div class="form-group required">
                <label for="titulo" class="control-label">Titulo</label>
                <input type="text" name="titulo" class="form-control" id="titulo" value="<?=isset($_POST['titulo'])?$_POST['titulo']:isset($aProducto['titulo'])?$aProducto['titulo']:false?>" placeholder="Ingrese Titulo" required autofocus>
              </div>

              <?php if($action == 'update'){ ?>
              <div class="form-group">
                <strong>Enlace permanente: </strong> <a href="<?=_global_siteurl?>producto/<?=$aProducto['slug']?>" target="_blank"><?=_global_siteurl?>producto/<?=$aProducto['slug']?></a> <i class="fas fa-external-link-alt"></i>
              </div>
              <?php } ?>

              <div class="row">
                <div class="form-group required col">
                  <label for="precio" class="control-label">Precio</label>
                  <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" name="precio" class="form-control" id="precio" value="<?=isset($_POST['precio'])?$_POST['precio']:isset($aProducto['precio'])?$aProducto['precio']:false?>" placeholder="Ingrese precio" required>
                  </div>
                </div>

                <div class="form-group required col">
                  <label for="codigo" class="control-label">Codigo</label>
                  <input type="text" name="codigo" class="form-control" id="codigo" value="<?=isset($_POST['codigo'])?$_POST['codigo']:isset($aProducto['codigo'])?$aProducto['codigo']:false?>" placeholder="Ingrese codigo" required>
                </div>
              </div>

              <div class="form-group">
                <label for="bajada" class="control-label">Bajada</label>
                <textarea name="bajada" class="form-control" id="bajada" rows="3"><?=isset($_POST['bajada'])?$_POST['bajada']:isset($aProducto['bajada'])?stripslashes($aProducto['bajada']):false?></textarea>
              </div>

              <div class="form-group required">
                <textarea name="descripcion" class="form-control" id="descripcion" rows="10" required ><?=isset($_POST['bajada'])?$_POST['bajada']:isset($aProducto['bajada'])?stripslashes($aProducto['descripcion']):false?></textarea>
              </div>

            </div> <!-- Fin col-8 -->

            <sidebar class="col-4">

              <div class="card border-primary mb-3">
                <div class="card-header">
                  <i class="fas fa-save"></i> Publicar
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <label for="publicada" class="control-label"><i class="fa fa-eye"></i> Estado</label><br>
                    <input type="radio" name="publicada" value="SI" <?=(isset($_POST['publicada'])&&$_POST['publicada']=='SI')||(isset($aProducto['publicada'])&&$aProducto['publicada']=='SI')||$action=='create'?'checked':''?> > Publicada
                    <input type="radio" name="publicada" value="NO" <?=(isset($_POST['publicada'])&&$_POST['publicada']=='NO')||(isset($aProducto['publicada'])&&$aProducto['publicada']=='NO')?'checked':''?> > Borrador
                  </div>
                  <?php if($action=='update'){ ?>
                  <p>
                    <i class="fas fa-calendar-alt"></i> Ingresado el: <b><?=fecha_completa($aProducto['add_date'])?></b>
                  </p>
                  <?php } ?>
                </div>
                <div class="card-footer text-right">
                  <?php if($action == 'update'){ ?>
                    <a href="#" class="delete-row float-left" data-id="<?=$aProducto['id']?>" alt="<?=$aProducto['titulo']?>" title="Eliminar" style="margin-top:6px;"><i class="fa fa-trash"></i> Eliminar</a>
                  <?php } ?>
                  <input name="procesar" type="submit" id="procesar" value="Guardar" class="btn btn-primary btn-sm" />
                </div>
              </div>
              
              <?php 
                $aCategorias = $oDB->categoria()->where("tipo = ?", "producto");
                if(intval($aCategorias->count("*")) > 0){
              ?>
              <div class="card border-primary mb-3">
                <div class="card-header">
                  <i class="fas fa-tags"></i> Categoria
                </div>
                <div class="card-body">
                  <div class="form-group">
                    
                    <select class="form-control input-sm" id="categoria_id" name="categoria_id" required>
                      <option value="">Seleccione</option>
                      <?php foreach ($aCategorias as $aCategoria) { ?>
                      <option value="<?=$aCategoria['id']?>" <?=(isset($_POST['categoria_id'])&&$_POST['categoria_id']==$aCategoria['id'])||(isset($aProducto['categoria_id'])&&$aProducto['categoria_id']==$aCategoria['id'])?'selected="selected"':''?>><?=$aCategoria['nombre']?></option>
                      <?php } ?>
                    </select>

                  </div>
                </div>
              </div>
              <?php } ?>

              <div class="card border-primary mb-3">
                <div class="card-header">
                  <i class="fas fa-image"></i> &nbsp;Imagen
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <label for="imagen" class="custom-file"> 
                      <input type="file" name="imagen" id="imagen" value="<?=isset($_POST['imagen'])?$_POST['imagen']:isset($aProducto['imagen'])?$aProducto['imagen']:false?>" class="custom-file-input" />
                      <span class="custom-file-control">Seleccionar imagen...</span>
                    </label>
                    <small class="help-block">Campo requerido. <br>590x415px es el tamaño sugerido para una mejor visualización en el sitio.</small>
                  </div>
                  <div class="form-group">
                    <input type="text" name="alt" class="form-control" id="alt" value="<?=isset($_POST['alt'])?$_POST['alt']:isset($aProducto['alt'])?$aProducto['alt']:false?>" placeholder="Texto alternativo de la imagen">
                  </div>
                </div>
                <?php if(isset($aProducto['imagen'])&&$aProducto['imagen']!=''){ ?>
                  <img src="<?=_global_produrl.$aProducto['imagen']?>" class="card-img-bottom">
                <?php } ?>
              </div>

              <div class="card border-primary">
                <div class="card-header">
                  <i class="fas fa-paperclip"></i> &nbsp;Archivo
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <label for="archivo" class="custom-file"> 
                      <input type="file" name="archivo" id="archivo" value="<?=isset($_POST['archivo'])?$_POST['archivo']:isset($aProducto['archivo'])?$aProducto['archivo']:false?>" class="custom-file-input" />
                      <span class="custom-file-control">Seleccionar archivo...</span>
                    </label>
                    <small class="help-block"></small>
                  </div>
                  <?php if(isset($aProducto['archivo'])&&$aProducto['archivo']!=''){ ?>
                    <div class="form-group">
                      <a href="<?=_global_fileurl.$aProducto['archivo']?>" class="badge badge-light" target="_blank">
                        <?=$aProducto['archivo']?>
                        <i class="fas fa-external-link-alt"></i>
                      </a>
                    </div>
                  <?php } ?>
                </div>
                
              </div>

            </sidebar> <!-- Fin sidebar -->

          </form>

        </div>

      </main>

    </div>

  </div> <!-- /container -->

  <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true" id="delete-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Se necesita confirmación</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-sm" id="modal-btn-si">Si</button>
          <button type="button" class="btn btn-secondary btn-sm" id="modal-btn-no" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
  
    <?php include('footer.php'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.js"></script>

    <!-- Otras funciones -->
    <script type="text/javascript">
      $(document).ready(function() {
        $('#descripcion').summernote({
          tabsize: 2,
          height: 300,
          minHeight: 100,
          maxHeight: 500
        });
        //Boton eliminar
        $('.card-footer').on( 'click', 'a.delete-row', function(event) {
          var id      = $(this).attr('data-id');
          var idOwner = $('#idOwner').text();
          var nombre  = $(this).attr('alt');
          var url     = 'frm-productos/procesar.php?action=delete';
          var options = {
            backdrop: 'static'
          };

          $('#delete-modal .modal-body').html("Esta seguro que desea eliminar <b>" + nombre + "</b>?");
          $('#delete-modal').modal(options);
          $("#modal-btn-si").on("click", function(){
            $.ajax({
              url: url,
              type: 'POST',
              dataType: 'json',
              data: { id: id, idOwner: idOwner },
              success: function(data){
                //alert(JSON.stringify(data, null, 4));
                if(data.success){
                  $(location).attr('href', 'proceso.php?op=panel/productos');                
                }
              },
              error: function(data){
                alert('Ocurrio un error eliminando el registro');
              }
            });
          
            event.preventDefault();
          });
        });

      });
    </script>

  </body>
</html>
