<?php

try
{

	$urlubic = "../";
	include_once($urlubic."func.includes/config.inc.php");

	/* 
	* Definir tabla y clave
	*/
	$sTable = "cliente";
	$sId 	= "id";

	//Nuevo registro
	if($_GET["action"] == "create"){
		$aData = [
			'nombre' => $_POST['nombre'],
			'apellido' => $_POST['apellido'],
			'slug' => to_prety_url($_POST['razon_social']),
			'razon_social' => $_POST['razon_social'],
			'rubro' => $_POST['rubro'],
			'email' => $_POST['email'],
			'idPais' => $_POST['idPais'],
			'idProvincia' => $_POST['idProvincia'],
			'idLocalidad' => $_POST['idLocalidad'],
			'rol_id' => $_POST['rol_id'],
			'confirmado' => $_POST['confirmado'],
			'edit_user' => $_POST['idOwner'],
			'edit_date' => date("Y-m-d H:m:s"),
			'eliminado' => 0
		];
		//+ Extra fields
		if(isset($_POST['password'])&&$_POST['password']!='')
			$adata['password'] = sha1($_POST['password']);

		if($_FILES['avatar']['name']!=""){
			$imagen = time()."_".$_FILES['avatar']['name'];
			$destino = _global_clientpath.$imagen ;
			move_uploaded_file($_FILES['avatar']['tmp_name'],$destino);
			$aData['avatar'] = $imagen;
		}

		if(isset($_POST['alt'])&&$_POST['alt']!='')
			$adata['alt'] = $_POST['alt'];

		//Insert record into database
		$ins = $oDB->cliente()->insert($aData);
		
		if(isset($ins['id']))
			header('location: ../proceso.php?op=alta/de/clientes&result=ok');
		else
			header('location: ../proceso.php?op=alta/de/clientes&result=bad');
	}
	//Editar registro
	else if($_GET["action"] == "update"){
		//Get ID
		$oRegistro = $oDB->cliente[secureParamToSql($_POST[$sId])]; 
		
		if($oRegistro){
			$aData = [
				'nombre' => $_POST['nombre'],
				'apellido' => $_POST['apellido'],
				'slug' => to_prety_url($_POST['razon_social']),
				'razon_social' => $_POST['razon_social'],
				'rubro' => $_POST['rubro'],
				'email' => $_POST['email'],
				'idPais' => $_POST['idPais'],
				'idProvincia' => $_POST['idProvincia'],
				'idLocalidad' => $_POST['idLocalidad'],
				'rol_id' => $_POST['rol_id'],
				'confirmado' => $_POST['confirmado'],
				'edit_user' => $_POST['idOwner'],
				'edit_date' => date("Y-m-d H:m:s"),
			];

			// Extra fields
			if(isset($_POST['password'])&&$_POST['password']!='')
				$adata['password'] = sha1($_POST['password']);

			if($_FILES['avatar']['name']!=""){
				$imagen = time()."_".$_FILES['avatar']['name'];
				$destino =  _global_clientpath.$imagen;
				move_uploaded_file($_FILES['avatar']['tmp_name'],$destino);
				$aData['avatar'] = $imagen;
			}

			if(isset($_POST['alt'])&&$_POST['alt']!='')
				$adata['alt'] = $_POST['alt'];

			//Update record in database
			$upd = $oRegistro->update($aData);
		}

		if($upd)
			header('location: ../proceso.php?op=panel/clientes&result=ok');
		else
			header('location: ../proceso.php?op=panel/clientes&result=bad');
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete"){
		//Defino el array donde voy a mandar los resultados de vuelta
		$aResult = [];

		//Get ID
		$oRegistro = $oDB->cliente[secureParamToSql($_POST['id'])];
		if($oRegistro){
			$aData = [
				'kill_user' => $_POST['idOwner'],		
				'kill_date' => date("Y-m-d H:m:s"),
				'eliminado' => 1
			];
			//"Delete" from database
			$aResult['success'] = $oRegistro->update($aData);
		} else {
			$aResult['success'] = false;
		}

		//Return data para comprobar si se elimino ya que se hace con AJAX necesitamos json_encode
		print(json_encode($aResult));
		
	}
	//getProvincia
	else if($_GET["action"]=="getProv"){
		$aProvincias = $oDB->tbprovincias->where("idPais = ?",secureParamToSql($_GET['idPais']));
        if($aProvincias)
	        echo json_encode($aProvincias);
	}
	//getLocalidad
	else if($_GET["action"]=="getLoc"){
		$aLocalidades = $oDB->tblocalidades->where("idProvincia = ?",secureParamToSql($_GET['idProvincia']));
        if($aLocalidades)
	        echo json_encode($aLocalidades);
	}

}
catch(Exception $ex)
{
    //Return error message
	$result = [];
	$result['Result'] = "ERROR";
	$result['Message'] = $ex->getMessage();
	print_r(json_encode($result));
}
	
?>