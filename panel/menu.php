<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
  <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="<?=_global_siteurl?>" target="_blank">
    <i class="fas fa-globe"></i>
    <?=_global_metaTitle?>
  </a>
  
  <form class="w-100">
    <input id="buscar" class="form-control form-control-dark w-100" type="text" placeholder="Buscar" aria-label="Buscar">
  </form>
  
  <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex px-3">
    <!-- <li class="nav-item dropdown">
      <a href="#" class="nav-item nav-link dropdown-toggle mr-md-2" data-toggle="dropdown">
        <?php if($_SESSION['AVATAR'] != ''){ ?>
          <img src="<?_global_siteurl?>panel/frm-usuarios/<?=$_SESSION['AVATAR'];?>" class="hidden-xs" width="20" height="20">
        <?php } else { ?>
          <i class="fas fa-user"></i>
        <?php } ?>
      </a>
      <div class="dropdown-menu dropdown-menu-right" role="menu">
        <a href="proceso.php?op=edicion/de/usuarios&amp;id=<?=$_SESSION['id']?>" class="dropdown-item">
          <i class="fas fa-id-card"></i>
          Editar mi perfil
        </a>
        <a href="proceso.php?op=logout" title="Salir" class="dropdown-item">
          <i class="fas fa-sign-out-alt"></i>
          Cerrar sesión
        </a>
      </div>
    </li>  -->
    <li class="nav-item text-nowrap">
      <a class="nav-link" href="proceso.php?op=logout">Cerrar sesión</a>
    </li>
  </ul>
  <span style="display:none;" id="idOwner"><?=$_SESSION['id']?></span>
</nav>
<!-- <header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-primary">
    <a class="navbar-brand" href="proceso.php?op=panel/administracion">Dashboard</a>
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        
        <li class="nav-item">
          <a class="nav-link" href="proceso.php?op=panel/clientes">
            <i class="fas fa-briefcase"></i>
            Clientes
          </a>
        </li>
        
        <?php if($_SESSION['NIVEL'] == 0){ ?>
          <li class="nav-item">
            <a class="nav-link" href="proceso.php?op=panel/usuarios">
              <i class="fas fa-users"></i>
              Usuarios
            </a>
          </li>
        <?php } ?>

        <li class="nav-item">
          <a class="nav-link" href="<?=_global_siteurl?>" target="_blank"> 
            <i class="fas fa-globe"></i>
            Visitar sitio
          </a>
        </li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
                
        <li class="nav-item dropdown">
          <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Bienvenido, <?=$_SESSION['USERNAME']?> 
            &nbsp; 
            <?php if($_SESSION['AVATAR'] != ''){ ?>
              <img src="<?_global_siteurl?>panel/frm-usuarios/<?=$_SESSION['AVATAR'];?>" class="hidden-xs" width="20" height="20">
            <?php } ?>
            <span style="display:none;" id="idOwner"><?=$_SESSION['id']?></span>
          </a>
          <div class="dropdown-menu" role="menu">
            <a href="proceso.php?op=edicion/de/usuarios&amp;id=<?=$_SESSION['id']?>" class="dropdown-item">
              <i class="fas fa-id-card"></i>
              Editar mi perfil
            </a>
            <a href="proceso.php?op=logout" title="Salir" class="dropdown-item">
              <i class="fas fa-sign-out-alt"></i>
              Cerrar sesión
            </a>
          </div>
        </li> 

      </ul>

    </div>
  </nav>
</header> -->