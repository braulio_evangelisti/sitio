<?php

  $urlubic = "";
  require('func.includes/seguridad.php');
  include_once("func.includes/config.inc.php");

  $op = secureParamToSql($_GET['op']);
  
  if($op == 'alta/de/slider') {
    $action   = 'create';
    $title    = 'Nuevo slide';
  } else if($op == 'edicion/de/slider') {
    $action    = 'update';
    $title     = 'Editar slide';
  
    $aSlide = $oDB->slider[secureParamToSql($_GET['id'])];  
  }

  //Definimos alert segun resultado de operacion ALTA
  if(isset($_GET['result']))
    switch ($_GET['result']) {
      case 'bad':
          $result = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button><i class='fas fa-exclamation-triangle'></i> Ocurrió un error durante la operación.</div>";
      break;
      case 'ok':
          $result = "<div class='alert alert-success alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button><i class='fas fa-check-circle'></i> La operación se ha realizado con éxito.</div>";
      break;
    }
  
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo _global_metaDesc; ?>">
    <meta name="keywords" content="<?php echo _global_metaKeys; ?>">
    <meta name="author" content="<?php echo _global_metaAuth; ?>">
    <link rel="icon" href="../img/favicon.ico">

    <title><?=$title?></title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    
    <!-- Autocomplete -->
    <link rel="stylesheet" href="js/EasyAutocomplete-1.3.5/easy-autocomplete.min.css"> 
    
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
    
  </head>

  <body>

    <?php include('menu.php'); ?>

    <div class="container-fluid">

      <div class="row">

        <?php include('left_nav.php'); ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="proceso.php?op=panel/administracion">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Slider</li>
            </ol>
          </nav>

          <h2>
            <?=$title?> 
            <a href="proceso.php?op=panel/slider" class="btn btn-secondary btn-sm float-right">Volver</a>
          </h2>

          <div>
            <?=isset($result)?$result:''?>

            <?php if(isset($aSlide['imagen'])&&$aSlide['imagen']!=''){ ?>
            <div class="form-group">
              <img src="<?=_global_sliderurl.$aSlide['imagen']?>" class="img-thumbnail img-fluid">
            </div>
            <?php } ?>

            <form action="frm-slider/procesar.php?action=<?=$action?>" class="form" method="POST" enctype="multipart/form-data" role="form">
              
              <input type="hidden" name="idOwner" class="idOwner" value="<?=$_SESSION['id']; ?>" />
              <?php if(isset($aSlide)){ ?>
              <input type="hidden" name="id" id="id" value="<?=$aSlide['id'];?>" />
              <input type="hidden" name="imagenOld" id="imagenOld" value="<?=$aSlide['imagen']; ?>" />
              <?php } ?>

              <div class="form-group">
                <label class="custom-file" for="imagen">
                  <input type="file" name="imagen" id="imagen" class="custom-file-input" value="<?=isset($_POST['imagen'])?$_POST['imagen']:isset($aSlide['imagen'])?$aSlide['imagen']:false?>" />
                  <span class="custom-file-control">Seleccionar imagen...</span>
                </label>
                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
              </div>

              <div class="form-group">
                <label for="alt">Texto alternativo</label>
                <input type="text" name="alt" class="form-control" id="alt" value="<?=isset($_POST['alt'])?$_POST['alt']:isset($aSlide['alt'])?$aSlide['alt']:false?>" placeholder="Texto alternativo de la imagen" >
              </div>
              
              <div class="form-group required">
                <label for="titulo">Título</label>
                <input type="text" name="titulo" class="form-control" id="titulo" value="<?=isset($_POST['titulo'])?$_POST['titulo']:isset($aSlide['titulo'])?$aSlide['titulo']:false?>" required >
              </div>

              <!-- <div class="form-group">
                <label for="txtSlide1" class="col-sm-4 control-label">Línea 1</label>
                <div class="col-sm-6">
                  <input type="text" name="txtSlide1" class="form-control" id="txtSlide1" value="<?=$aSlide['txtSlide1']; ?>" required >
                </div>
              </div>

              <div class="form-group">
                <label for="txtSlide2" class="col-sm-4 control-label">Línea 2</label>
                <div class="col-sm-6">
                  <input type="text" name="txtSlide2" class="form-control" id="txtSlide2" value="<?=$aSlide['txtSlide2']; ?>" >
                </div>
              </div>

              <div class="form-group">
                <label for="txtSlide3" class="col-sm-4 control-label">Línea 3</label>
                <div class="col-sm-6">
                  <input type="text" name="txtSlide3" class="form-control" id="txtSlide3" value="<?=$aSlide['txtSlide3']; ?>" >
                </div>
              </div>

              <div class="form-group">
                <label for="txtSlide4" class="col-sm-4 control-label">Línea 4</label>
                <div class="col-sm-6">
                  <input type="text" name="txtSlide4" class="form-control" id="txtSlide4" value="<?=$aSlide['txtSlide4']; ?>" >
                </div>
              </div> -->

              <div class="form-group required">
                <label for="orden">Orden</label>
                <input type="text" name="orden" class="form-control" id="orden" value="<?=isset($_POST['orden'])?$_POST['orden']:isset($aSlide['orden'])?$aSlide['orden']:false?>" required>
              </div>

              <div class="form-group">
                <label for="publicada">Publicar</label><br>
                <div class="form-check form-check-inline">
                  <label class="form-check-label">
                    <input type="radio" name="publicada" value="SI" 
                      <?=((isset($_POST['publicada'])&&$_POST['publicada']=='SI')||(isset($aSlide['publicada'])&&$aSlide['publicada']=='SI')||$action=='create')?'checked':''?> class="form-check-input"> 
                    SI
                  </label>
                </div>
                <div class="form-check form-check-inline">
                  <label class="form-check-label">
                    <input type="radio" name="publicada" value="NO" 
                      <?=((isset($_POST['publicada'])&&$_POST['publicada']=='NO')||(isset($aSlide['publicada'])&&$aSlide['publicada']=='NO'))?'checked':''?> class="form-check-input" > 
                    NO
                  </label>
                </div>
              </div>

              <div class="form-group">
                <input name="procesar" type="submit" id="procesar" value="Guardar" class="btn btn-primary float-right" />
              </div>

              </form>

            </div>

          </main>

        </div>

      </div> <!-- /container -->
      
    <?php include('footer.php'); ?>    

    <!-- Otras funciones -->

  </body>
</html>
