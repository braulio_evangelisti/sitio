<?php

  $urlubic = "";
  require($urlubic.'func.includes/seguridad.php');
  include_once($urlubic."func.includes/config.inc.php");

  $aSlider = $oDB->slider()->where("eliminado = ?", 0)->order('orden ASC');
  
  //Definimos alert segun resultado de operacion EDICION
  if(isset($_GET['result']))
    switch ($_GET['result']) {
      case 'bad':
        $result = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button><i class='fas fa-exclamation-triangle'></i> Ocurrió un error durante la operación.</div>";
      break;
      case 'ok':
        $result = "<div class='alert alert-success alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button><i class='fas fa-check-circle'></i> La operación se ha realizado con éxito.</div>";
      break;
    }
  
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?=_global_metaDesc; ?>">
    <meta name="keywords" content="<?=_global_metaKeys; ?>">
    <meta name="author" content="<?=_global_metaAuth; ?>">
    <link rel="icon" href="../img/favicon.ico">

    <title>Gestión de slider</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap4.min.css">
    
    <!-- Autocomplete -->
    <link rel="stylesheet" href="js/EasyAutocomplete-1.3.5/easy-autocomplete.min.css"> 
    
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
    
  </head>

  <body>

    <?php include('menu.php'); ?>

    <div class="container-fluid">

      <div class="row">

        <?php include('left_nav.php'); ?>
        
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="proceso.php?op=panel/administracion">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Slider</li>
            </ol>
          </nav>

          <h2>Slider <small></small>
            <a href="proceso.php?op=alta/de/slider" class="btn btn-primary float-right">
              <i class="fas fa-plus"></i>
              Nuevo
            </a>
          </h2>

          <div>
            <?=isset($result)?$result:'';?>
            <!-- <p class="text-right">Buscar: <input id="filter" type="text"></p> -->
            <table class="table table-sm table-hover dt-responsive nowrap" cellspacing="0" width="100%">

                <thead>
                  <tr>
                    <th width="20">#</th>
                    <th width="100">Imagen</th>
                    <th >Título</th>
                    <th width="60">Orden</th>
                    <th width="50">Estado</th>
                    <th width="30">Acciones</th>
                  </tr>
                </thead>

                <tbody>

                  <?php 
                  if($aSlider){
                    foreach($aSlider as $slide){
                  ?>
                    <tr>
                      <td><?=$slide['id']?></td>
                      <td>
                          <?php if($slide['imagen'] != ''){ ?>
                            <img src="<?=_global_sliderurl.$slide['imagen']?>" height="50" class="img-thumbnail">
                          <?php } else { ?>
                            Sin imagen
                          <?php } ?>
                      </td>
                      <td><?=ucfirst($slide['titulo'])?></td>
                      <td><?=$slide['orden']?></td>
                      <td><?=($slide['publicada']=='SI')?'<span class="badge badge-success">Publicado</span>':'<span class="badge badge-danger">Borrador</span>'?></td>
                      <td>
                        <a href="proceso.php?op=edicion/de/slider&amp;id=<?=$slide['id']?>" class="btn btn-primary btn-sm" title="Editar"><i class="fas fa-pencil-alt"></i></a>
                        <a href="#" class="btn btn-danger btn-sm delete-row" data-id="<?=$slide['id']?>" data-name="<?=$slide['titulo']?>" title="Eliminar"><i class="fas fa-trash"></i></a>
                      </td>
                    </tr>
                  <?php 
                      }
                  } else {
                  ?>
                    <tr>
                      <td colspan="5" class="text-center">
                        No se encontraron registros
                      </td>
                    </tr>
                  <?php } ?>        
                </tbody>
              </table>

          </div>

        </main>

      </div>

    </div> <!-- /container -->

    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true" id="delete-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Se necesita confirmación</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-sm" id="modal-btn-si">Si</button>
            <button type="button" class="btn btn-secondary btn-sm" id="modal-btn-no" data-dismiss="modal">No</button>
          </div>
        </div>
      </div>
    </div>

    <?php include('footer.php'); ?>  

    <!-- Otras funciones -->
    <script>
      $(document).ready(function(){
        var table = $('.table').DataTable({
          "columns": [
            { "orderable": true },
            { "orderable": false },
            { "orderable": true },
            { "orderable": true },
            { "orderable": false },
            { "orderable": false }
          ],
          "order": [[ 3, "asc" ]],
          "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
          }
        });

        //Boton eliminar de cada fila
        $('.table').on( 'click', 'a.delete-row', function(event) {

          var id      = $(this).attr('data-id');
          var idOwner = $('#idOwner').text();
          var nombre  = $(this).attr('data-name');
          var url     = 'frm-slider/procesar.php?action=delete';
          var tr      = $(this).closest('tr');
          var options = {
            backdrop: 'static'
          };

          $('#delete-modal .modal-body').html("Esta seguro que desea eliminar a <b>" + nombre + "</b>?");
          $('#delete-modal').modal(options);
          $("#modal-btn-si").on("click", function(){
            $.ajax({
              url: url,
              type: 'POST',
              dataType: 'json',
              data: { id: id, idOwner: idOwner },
              success: function(data){
                //alert(JSON.stringify(data, null, 4));
                if(data.success){
                  tr.fadeOut(1000,function(){ 
                    tr.remove();                    
                  }); 
                  $("#delete-modal").modal('hide');
                }
              },
              error: function(data){
                alert('Ocurrio un error eliminando el registro');
                $("#delete-modal").modal('hide');
              }
            });
          });
          event.preventDefault();
        });
      });
    </script>

  </body>
</html>
