<?php

try
{

	$urlubic = "../";

	include_once($urlubic."func.includes/config.inc.php");

	/* 
	* Definir tabla y clave
	*/
	$sTable = "slider";
	$sId 	= "id";

	//Nuevo registro
	if($_GET["action"] == "create"){
		$aData = [
			'titulo' => $_POST['titulo'],
			'alt' => $_POST['alt'],
			'orden' => $_POST['orden'],
			'publicada' => $_POST['publicada'],
			'add_user' => $_POST['idOwner'],
			'add_date' => date("Y-m-d H:m:s"),
			'eliminado' => 0
		];

		//+ Extra fields
		if($_FILES['imagen']['name']  != ""){
			$imagen = time()."_".$_FILES['imagen']['name'];
			$destino =  _global_sliderpath.$imagen;
			move_uploaded_file($_FILES['imagen']['tmp_name'],$destino);
			$aData['imagen'] = $imagen;
		}

		//Insert record into database
		$ins = $oDB->slider()->insert($aData);
		
		if(isset($ins['id']))
			header('location: ../proceso.php?op=alta/de/slider&result=ok');
		else
			header('location: ../proceso.php?op=alta/de/slider&result=bad');
	}
	//Editar registro
	else if($_GET["action"] == "update"){

		//Get ID
		$oRegistro = $oDB->slider[secureParamToSql($_POST[$sId])]; 
		
		if($oRegistro){
			$aData = [
				'alt' => $_POST['alt'],
				'titulo' => $_POST['titulo'],
				'orden' => $_POST['orden'],
				'publicada' => $_POST['publicada'],
				'edit_user' => $_POST['idOwner'],
				'edit_date' => date("Y-m-d H:m:s"),
			];

			//+ Extra fields
			if($_FILES['imagen']['name']  != ""){
				$imagen = time()."_".$_FILES['imagen']['name'];
				$destino =  _global_sliderpath.$imagen;
				move_uploaded_file($_FILES['imagen']['tmp_name'],$destino);
				$aData['imagen'] = $imagen;
			} else {
				$aData['imagen'] = $_POST['imagenOld'];
			}

			//Update record in database
			$upd = $oRegistro->update($aData);
		}

		if($upd)
			header('location: ../proceso.php?op=panel/slider&result=ok');
		else
			header('location: ../proceso.php?op=panel/slider&result=bad');
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete"){

		//Defino el array donde voy a mandar los resultados de vuelta
		$aResult = [];

		//Get ID
		$oRegistro = $oDB->slider[secureParamToSql($_POST['id'])];
		if($oRegistro){
			$aData = [
				'kill_user' => $_POST['idOwner'],		
				'kill_date' => date("Y-m-d H:m:s"),
				'eliminado' => 1
			];
			//"Delete" from database
			$aResult['success'] = $oRegistro->update($aData);
		} else {
			$aResult['success'] = false;
		}

		//Return data
		print(json_encode($aResult));
		
	}

}
catch(Exception $ex)
{
    //Return error message
	$aResult = [];
	$aResult['Result'] = "ERROR";
	$aResult['Message'] = $ex->getMessage();
	print_r(json_encode($aResult));
}
	
?>