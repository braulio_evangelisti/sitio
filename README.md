[![build][build-bitbucket]][build-bitbucket-url]

# README #

- Panel

### What is this repository for? ###

* Website base structure w/ an admin panel.
* v1.0.3

### How do I get set up? ###

* Clone repo
* Configuration
* Start composer inside /panel
* MySql Database

### Who do I talk to? ###

* bevangelisti
* [GrafitoDD](http://grafitodd.com)

[build-bitbucket]: https://img.shields.io/bitbucket/pipelines/braulio_evangelisti/sitio.svg
[build-bitbucket-url]: https://bitbucket.org/braulio_evangelisti/sitio/addon/pipelines/deployments